﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(TrackerContext roles)
            : base(roles)
        {

        }

        public IQueryable<Role> GetAllWithDetails() => _entities.Include(e => e.Employments);

        public async Task<Role> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
