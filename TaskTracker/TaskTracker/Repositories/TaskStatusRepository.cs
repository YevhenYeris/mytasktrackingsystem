﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class TaskStatusRepository : Repository<ProjectTaskStatus>, ITaskStatusRepository
    {
        public TaskStatusRepository(TrackerContext taskStatuses)
            : base(taskStatuses)
        {

        }

        public IQueryable<ProjectTaskStatus> GetAllWithDetails() => _entities.Include(e => e.Tasks);

        public async Task<ProjectTaskStatus> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}

