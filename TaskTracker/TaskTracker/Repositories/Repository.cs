﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbSet<TEntity> _entities;

        public Repository(TrackerContext context)
        {
            _ = context ?? throw new ArgumentNullException(nameof(context));

            _entities = context.Set<TEntity>();
        }

        public async Task AddAsync(TEntity entity)
        {
            await _entities.AddAsync(entity);
        }

        public void Delete(TEntity entity)
        {
            _entities.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await GetByIdAsync(id);
            Delete(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _entities;
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _entities.FindAsync(id);
        }

        public void Update(TEntity entity)
        {
            _entities.Attach(entity);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            var entity = await _entities.FindAsync(id);
            return entity != null;
        }
    }
}
