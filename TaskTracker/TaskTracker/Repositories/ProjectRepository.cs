﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(TrackerContext projects)
            : base(projects)
        {

        }

        public IQueryable<Project> GetAllWithDetails() => _entities.Include(e => e.Category)
                                                                   .ThenInclude(e => e.Projects)
                                                                   .Include(e => e.Employments)
                                                                   .Include(e => e.Tasks);

        public async Task<Project> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
