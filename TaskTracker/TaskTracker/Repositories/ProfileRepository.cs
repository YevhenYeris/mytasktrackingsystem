﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class ProfileRepository : Repository<UserProfile>, IProfileRepository
    {
        public ProfileRepository(TrackerContext profiles)
            : base(profiles)
        {

        }

        public IQueryable<UserProfile> GetAllWithDetails() => _entities.Include(e => e.Categories)
                                                                       .ThenInclude(e => e.Projects)
                                                                       .ThenInclude(e => e.Tasks)
                                                                       .Include(e => e.Employments)
                                                                       .ThenInclude(e => e.Project)
                                                                       .ThenInclude(e => e.Tasks);

        public async Task<UserProfile> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
