﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class AssignmentRepository : Repository<Assignment>, IAssignmentRepository
    {
        public AssignmentRepository(TrackerContext assignments)
            : base(assignments)
        {

        }

        public IQueryable<Assignment> GetAllWithDetails() => _entities.Include(e => e.Employment)
                                                                      .Include(e => e.Task);

        public async Task<Assignment> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
