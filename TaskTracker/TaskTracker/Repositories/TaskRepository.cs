﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class TaskRepository : Repository<ProjectTask>, ITaskRepository
    {
        public TaskRepository(TrackerContext tasks)
            : base(tasks)
        {

        }

        public IQueryable<ProjectTask> GetAllWithDetails() => _entities.Include(e => e.Project)
                                                                       .ThenInclude(e => e.Category)
                                                                       .Include(e => e.TaskStatus)
                                                                       .Include(e => e.TaskUpdates)
                                                                       .Include(e => e.Assignments)
                                                                       .ThenInclude(a => a.Employment);

        public async Task<ProjectTask> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
