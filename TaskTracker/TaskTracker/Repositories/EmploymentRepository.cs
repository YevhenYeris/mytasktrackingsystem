﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class EmploymentRepository : Repository<Employment>, IEmploymentRepository
    {
        public EmploymentRepository(TrackerContext employments)
            : base(employments)
        {

        }

        public IQueryable<Employment> GetAllWithDetails() => _entities.Include(e => e.Project)
                                                                      .Include(e => e.Role)
                                                                      .Include(e => e.User)
                                                                      .Include(e => e.Assignments);

        public async Task<Employment> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
