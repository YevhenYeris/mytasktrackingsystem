﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class TaskUpdateRepository : Repository<TaskUpdate>, ITaskUpdateRepository
    {
        public TaskUpdateRepository(TrackerContext taskUpdates)
            : base(taskUpdates)
        {

        }

        public IQueryable<TaskUpdate> GetAllWithDetails() => _entities.Include(e => e.Employment)
                                                                      .Include(e => e.Task);

        public async Task<TaskUpdate> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
