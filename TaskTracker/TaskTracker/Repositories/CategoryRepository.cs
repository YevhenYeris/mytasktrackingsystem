﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;

namespace Data.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(TrackerContext categories)
            : base(categories)
        {

        }

        public IQueryable<Category> GetAllWithDetails() => _entities.Include(e => e.Projects)
                                                                    .ThenInclude(e => e.Tasks)
                                                                    .Include(e => e.User);

        public async Task<Category> GetByIdWithDetailsAsync(int id) => await GetAllWithDetails().FirstOrDefaultAsync(e => e.Id == id);
    }
}
