﻿using System;
using System.Data.Common;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;
using Data.Configuration;

namespace Data
{
    public class TrackerContext : DbContext
    {
        public TrackerContext()
        {

        }

        public TrackerContext(DbContextOptions<TrackerContext> options)
            :base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            new AssignmentConfig().Configure(builder.Entity<Assignment>());
            new CategoryConfig().Configure(builder.Entity<Category>());
            new EmploymentConfig().Configure(builder.Entity<Employment>());
            new ProjectConfig().Configure(builder.Entity<Project>());
            new TaskConfig().Configure(builder.Entity<ProjectTask>());
            new TaskStatusConfig().Configure(builder.Entity<ProjectTaskStatus>());
            new TaskUpdateConfig().Configure(builder.Entity<TaskUpdate>());
            new RoleConfig().Configure(builder.Entity<Role>());
            new UserProfileConfig().Configure(builder.Entity<UserProfile>());
        }
    }
}
