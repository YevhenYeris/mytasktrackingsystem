﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;

namespace Data.Configuration
{
    public class AssignmentConfig : IEntityTypeConfiguration<Assignment>
    {
        public void Configure(EntityTypeBuilder<Assignment> builder)
        {
            builder.ToTable("Assignments");

            builder.Property(e => e.Description)
                   .HasMaxLength(500);

            builder.HasOne(e => e.Employment)
                   .WithMany(e => e.Assignments)
                   .HasForeignKey(e => e.EmploymentId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .IsRequired();

            builder.HasOne(e => e.Task)
                   .WithMany(e => e.Assignments)
                   .HasForeignKey(e => e.TaskId)
                   .OnDelete(DeleteBehavior.NoAction)
                   .IsRequired();
        }
    }
}
