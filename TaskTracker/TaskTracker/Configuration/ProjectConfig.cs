﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;

namespace Data.Configuration
{
    public class ProjectConfig : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Projects");

            builder.Property(e => e.ProjectName)
                   .HasMaxLength(25)
                   .IsRequired();

            builder.Property(e => e.Description)
                   .HasMaxLength(1000)
                   .IsRequired(false);

            builder.Property(e => e.StartTime)
                   .IsRequired();

            builder.Property(e => e.DueTime)
                   .IsRequired();

            builder.HasOne(e => e.Category)
                   .WithMany(e => e.Projects)
                   .HasForeignKey(e => e.CategoryId)
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired();

            builder.Property(e => e.Color)
                   .HasDefaultValue("#ffffff");
        }
    }
}
