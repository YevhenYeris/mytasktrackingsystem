﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;

namespace Data.Configuration
{
    public class TaskStatusConfig : IEntityTypeConfiguration<ProjectTaskStatus>
    {
        public void Configure(EntityTypeBuilder<ProjectTaskStatus> builder)
        {
            builder.ToTable("TaskStatuses");

            builder.Property(e => e.StatusName)
                   .HasMaxLength(30)
                   .IsRequired();
        }
    }
}
