﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;

namespace Data.Configuration
{
    public class UserProfileConfig : IEntityTypeConfiguration<UserProfile>
    {
        public void Configure(EntityTypeBuilder<UserProfile> builder)
        {
            builder.ToTable("UserProfiles");

            builder.Property(e => e.UserId)
                   .IsRequired();

            builder.Property(e => e.Email)
                   .HasMaxLength(320)
                   .IsRequired();

            builder.Property(e => e.UserName)
                   .HasMaxLength(30)
                   .IsRequired();
        }
    }
}
