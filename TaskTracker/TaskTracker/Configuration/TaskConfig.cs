﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;

namespace Data.Configuration
{
    public class TaskConfig : IEntityTypeConfiguration<ProjectTask>
    {
        public void Configure(EntityTypeBuilder<ProjectTask> builder)
        {
            builder.ToTable("Tasks");

            builder.Property(e => e.TaskName)
                   .HasMaxLength(25)
                   .IsRequired();

            builder.Property(e => e.Description)
                   .HasMaxLength(1500);

            builder.HasOne(e => e.Project)
                   .WithMany(e => e.Tasks)
                   .HasForeignKey(e => e.ProjectId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .IsRequired();

            builder.HasOne(e => e.TaskStatus)
                   .WithMany(e => e.Tasks)
                   .OnDelete(DeleteBehavior.ClientSetNull)
                   .IsRequired(false);

            builder.Property(e => e.StartTime)
                   .IsRequired();

            builder.Property(e => e.DueTime)
                   .IsRequired();

            builder.Property(e => e.IsFinished)
                   .IsRequired();
        }
    }
}
