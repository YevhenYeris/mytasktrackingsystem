﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Data.Entities;

namespace Data.Configuration
{
    public class TaskUpdateConfig : IEntityTypeConfiguration<TaskUpdate>
    {
        public void Configure(EntityTypeBuilder<TaskUpdate> builder)
        {
            builder.ToTable("TaskUpdates");

            builder.Property(e => e.UpdateTime)
                   .IsRequired();

            builder.Property(e => e.Description)
                   .HasMaxLength(500)
                   .IsRequired();

            builder.HasOne(e => e.Employment)
                    .WithMany(e => e.TaskUpdates)
                    .HasForeignKey(e => e.EmploymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .IsRequired(false);

            builder.HasOne(e => e.Task)
                   .WithMany(e => e.TaskUpdates)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasForeignKey(e => e.TaskId);
        }
    }
}
