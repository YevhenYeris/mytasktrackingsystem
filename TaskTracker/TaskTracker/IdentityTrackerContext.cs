﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class IdentityTrackerContext: IdentityDbContext
    {
        public IdentityTrackerContext(DbContextOptions<IdentityTrackerContext> options)
            :base(options)
        {

        }
    }
}
