﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Data.Entities
{
    public class UserProfile : BaseEntity
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public ICollection<Employment> Employments { get; set; }

        public ICollection<Category> Categories { get; set; }
    }
}