﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Category : BaseEntity
    {
        public string CategoryName { get; set; }

        public int UserId { get; set; }

        public UserProfile User { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
