﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class ProjectTask : BaseEntity
    {
        public string TaskName { get; set; }

        public string Description { get; set; }

        public int ProjectId { get; set; }

        public int TaskStatusId { get; set; }

        public bool IsFinished { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime FinishTime { get; set; }

        public DateTime DueTime { get; set; }

        public Project Project { get; set; }

        public ProjectTaskStatus TaskStatus { get; set; }

        public ICollection<Assignment> Assignments { get; set; }

        public ICollection<TaskUpdate> TaskUpdates { get; set; }
    }
}
