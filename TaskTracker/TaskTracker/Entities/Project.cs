﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Project : BaseEntity
    {
        public string ProjectName { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime DueTime { get; set; }

        public DateTime FinishTime { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public ICollection<Employment> Employments { get; set; }

        public ICollection<ProjectTask> Tasks { get; set; }
    }
}
