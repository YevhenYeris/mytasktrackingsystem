﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Assignment : BaseEntity
    {
        public int EmploymentId { get; set; }

        public int TaskId { get; set; }

        public string Description { get; set; }

        public Employment Employment { get; set; }

        public ProjectTask Task { get; set; }
    }
}
