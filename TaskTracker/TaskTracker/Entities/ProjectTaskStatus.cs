﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class ProjectTaskStatus : BaseEntity
    {
        public string StatusName { get; set; }

        public ICollection<ProjectTask> Tasks { get; set; }
    }
}
