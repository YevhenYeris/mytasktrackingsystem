﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Employment : BaseEntity
    {
        public int UserId { get; set; }

        public int ProjectId { get; set; }

        public int RoleId { get; set; }

        public bool RecieveNotifications { get; set; }

        public UserProfile User { get; set; }

        public Project Project { get; set; }

        public Role Role { get; set; }

        public ICollection<Assignment> Assignments { get; set; }

        public ICollection<TaskUpdate> TaskUpdates { get; set; }
    }
}
