﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Role : BaseEntity
    {
        public string RoleName { get; set; }

        public string Color { get; set; }

        public ICollection<Employment> Employments { get; set; }
    }
}
