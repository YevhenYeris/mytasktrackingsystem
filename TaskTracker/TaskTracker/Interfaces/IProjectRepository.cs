﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
        public IQueryable<Project> GetAllWithDetails();

        public Task<Project> GetByIdWithDetailsAsync(int id);
    }
}