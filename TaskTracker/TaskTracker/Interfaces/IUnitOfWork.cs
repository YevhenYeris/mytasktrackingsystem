﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;

namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        public IAssignmentRepository AssignmentRepository { get; }

        public ICategoryRepository CategoryRepository { get; }

        public IEmploymentRepository EmploymentRepository { get; }

        public IProfileRepository ProfileRepository { get; }

        public IProjectRepository ProjectRepository { get; }

        public IRoleRepository RoleRepository { get; }

        public ITaskRepository TaskRepository { get; }

        public ITaskStatusRepository TaskStatusRepository { get; }

        public ITaskUpdateRepository TaskUpdateRepository { get; }

        public Task<int> SaveChangesAsync(); 
    }
}
