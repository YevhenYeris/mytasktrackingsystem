﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface ITaskRepository : IRepository<ProjectTask>
    {
        public IQueryable<ProjectTask> GetAllWithDetails();

        public Task<ProjectTask> GetByIdWithDetailsAsync(int id);
    }
}