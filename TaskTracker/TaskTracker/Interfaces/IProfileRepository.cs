﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IProfileRepository : IRepository<UserProfile>
    {
        public IQueryable<UserProfile> GetAllWithDetails();

        public Task<UserProfile> GetByIdWithDetailsAsync(int id);
    }
}