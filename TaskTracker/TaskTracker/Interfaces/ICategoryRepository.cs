﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
        public IQueryable<Category> GetAllWithDetails();

        public Task<Category> GetByIdWithDetailsAsync(int id);
    }
}