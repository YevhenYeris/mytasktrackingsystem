﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface ITaskStatusRepository : IRepository<ProjectTaskStatus>
    {
        public IQueryable<ProjectTaskStatus> GetAllWithDetails();

        public Task<ProjectTaskStatus> GetByIdWithDetailsAsync(int id);
    }
}
