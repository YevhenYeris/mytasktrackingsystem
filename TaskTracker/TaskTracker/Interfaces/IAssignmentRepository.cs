﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IAssignmentRepository : IRepository<Assignment>
    {
        public IQueryable<Assignment> GetAllWithDetails();

        public Task<Assignment> GetByIdWithDetailsAsync(int id);
    }
}