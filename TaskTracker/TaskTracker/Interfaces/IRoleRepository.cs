﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        public IQueryable<Role> GetAllWithDetails();

        public Task<Role> GetByIdWithDetailsAsync(int id);
    }
}
