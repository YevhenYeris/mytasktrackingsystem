﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IEmploymentRepository : IRepository<Employment>
    {
        public IQueryable<Employment> GetAllWithDetails();

        public Task<Employment> GetByIdWithDetailsAsync(int id);
    }
}