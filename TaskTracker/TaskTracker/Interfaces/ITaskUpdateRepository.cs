﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Interfaces
{
    public interface ITaskUpdateRepository : IRepository<TaskUpdate>
    {
        public IQueryable<TaskUpdate> GetAllWithDetails();

        public Task<TaskUpdate> GetByIdWithDetailsAsync(int id);
    }
}