﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Repositories;
using Data.Entities;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TrackerContext _context;

        public UnitOfWork(TrackerContext context)
        {
            _ = context ?? throw new ArgumentNullException(nameof(context));

            AssignmentRepository = new AssignmentRepository(context);
            CategoryRepository = new CategoryRepository(context);
            EmploymentRepository = new EmploymentRepository(context);
            ProfileRepository = new ProfileRepository(context);
            ProjectRepository = new ProjectRepository(context);
            RoleRepository = new RoleRepository(context);
            TaskRepository = new TaskRepository(context);
            TaskStatusRepository = new TaskStatusRepository(context);
            TaskUpdateRepository = new TaskUpdateRepository(context);

            _context = context;
        }

        public IAssignmentRepository AssignmentRepository { get; }

        public ICategoryRepository CategoryRepository { get; }

        public IEmploymentRepository EmploymentRepository { get; }

        public IProfileRepository ProfileRepository { get; }

        public IProjectRepository ProjectRepository { get; }

        public IRoleRepository RoleRepository { get; }

        public ITaskRepository TaskRepository { get; }

        public ITaskStatusRepository TaskStatusRepository { get; }

        public ITaskUpdateRepository TaskUpdateRepository { get; }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
