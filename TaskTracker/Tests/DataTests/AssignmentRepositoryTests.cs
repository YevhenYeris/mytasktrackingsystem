﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Data;
using Data.Entities;
using Data.Repositories;

namespace Tests.DataTests
{
    public class AssignmentRepositoryTests
    {
        [Fact]
        public async Task GetAll_ReturnsAllEntities()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);
            var expected = AllAssignments;

            //act
            var actual = repo.GetAll() as IEnumerable<Assignment>;

            //assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetByIdAsync_ExistingId_ReturnsEntity(int id)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);
            var expected = AllAssignments.FirstOrDefault(e => e.Id == id);

            //act
            var actual = await repo.GetByIdAsync(id);

            //assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Fact]
        public async Task AddAsync_Category_CountIncreasedByOne()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);

            var assignment = new Assignment { EmploymentId = 1, TaskId = 1 };
            var expected = repo.GetAll().Count() + 1;

            //act
            await repo.AddAsync(assignment);
            await context.SaveChangesAsync();
            var actual = repo.GetAll().Count();

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task Update_ChangedEntity_EntityUpdated()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);

            var expected = new Assignment { Id = 1, EmploymentId = 1, TaskId = 1, Description = "123" };

            //act
            repo.Update(expected);
            context.SaveChanges();
            var actual = await repo.GetByIdAsync(expected.Id);


            //assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Fact]
        public async Task Delete_ExistingEntity_EntityDeleted()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);

            var assignment = repo.GetAll().FirstOrDefault();
            var expected = repo.GetAll().Count() - 1;

            //act
            repo.Delete(assignment);
            await context.SaveChangesAsync();
            var actual = repo.GetAll().Count();

            //assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task DeleteByIdAsync_ValidId_EntityDeleted(int id)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);

            var expected = repo.GetAll().Count() - 1;

            //act
            await repo.DeleteByIdAsync(id);
            await context.SaveChangesAsync();
            var actual = repo.GetAll().Count();

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task GetAllWithDetails_ReturnsAllWithDetails()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);

            var expected = AllAssignments;

            //act
            var actual = repo.GetAllWithDetails();

            //assert
            Assert.Equal(expected, actual, Comparer);
            Assert.Equal(ExpectedEmployments.Distinct().OrderBy(e => e.Id), actual.Select(e => e.Employment).Distinct().OrderBy(e => e.Id), new EmploymentEqualityComparer());
            Assert.Equal(ExpectedTasks.Distinct().OrderBy(e => e.Id), actual.Select(e => e.Task).Distinct().OrderBy(e => e.Id), new ProjectTaskEqualityComparer());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetByIdWithDetailsAsync_ValidId_ReturnsEntityWithDetails(int id)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new AssignmentRepository(context);

            var expected = AllAssignments.Where(e => e.Id == id).FirstOrDefault();
            expected.Employment = ExpectedEmployments.FirstOrDefault(e => e.Id == expected.EmploymentId);
            expected.Task = ExpectedTasks.FirstOrDefault(e => e.Id == expected.TaskId);

            //act
            var actual = await repo.GetByIdWithDetailsAsync(id);

            //assert
            Assert.Equal(expected, actual, Comparer);
            Assert.Equal(expected.Employment, actual.Employment, new EmploymentEqualityComparer());
            Assert.Equal(expected.Task, actual.Task, new ProjectTaskEqualityComparer());
        }

        private AssignmentEqualityComparer Comparer => new AssignmentEqualityComparer();

        private IEnumerable<Assignment> AllAssignments =>
            new[]
            {
                new Assignment() { Id = 1, TaskId = 1, EmploymentId = 3},
                new Assignment() { Id = 2, TaskId = 1, EmploymentId = 4},
                new Assignment() { Id = 3, TaskId = 1, EmploymentId = 5},
                new Assignment() { Id = 4, TaskId = 2, EmploymentId = 1},
                new Assignment() { Id = 5, TaskId = 4, EmploymentId = 6},
                new Assignment() { Id = 6, TaskId = 5, EmploymentId = 7},
                new Assignment() { Id = 7, TaskId = 5, EmploymentId = 8},
                new Assignment() { Id = 8, TaskId = 5, EmploymentId = 9},
                new Assignment() { Id = 9, TaskId = 7, EmploymentId = 10},
                new Assignment() { Id = 10, TaskId = 8, EmploymentId = 5},
                new Assignment() { Id = 11, TaskId = 8, EmploymentId = 2},
                new Assignment() { Id = 12, TaskId = 8, EmploymentId = 3},
                new Assignment() { Id = 13, TaskId = 8, EmploymentId = 4}
            };

        private IEnumerable<ProjectTask> ExpectedTasks =>
            new[]
            {
                new ProjectTask() { Id = 1, ProjectId = 1, TaskName = "Raise troops", TaskStatusId = 3},
                new ProjectTask() { Id = 2, ProjectId = 1, TaskName = "Gather commanders", TaskStatusId = 2},
                new ProjectTask() { Id = 4, ProjectId = 2, TaskName = "Send the order", TaskStatusId = 5 },
                new ProjectTask() { Id = 5, ProjectId = 2, TaskName = "Kill the jedi", TaskStatusId = 4},
                new ProjectTask() { Id = 7, ProjectId = 3, TaskName = "Stop the jedi", TaskStatusId = 6},
                new ProjectTask() { Id = 8, ProjectId = 3, TaskName = "Abandon the ship", TaskStatusId = 3}
            };

        private IEnumerable<Employment> ExpectedEmployments =>
            new[]
            {
                new Employment() { Id = 1, ProjectId = 1, RoleId = 1, UserId = 4},
                new Employment() { Id = 2, ProjectId = 1, RoleId = 2, UserId = 3 },
                new Employment() { Id = 3, ProjectId = 1, RoleId = 3, UserId = 6},
                new Employment() { Id = 4, ProjectId = 1, RoleId = 3, UserId = 7},
                new Employment() { Id = 5, ProjectId = 1, RoleId = 3, UserId = 8},
                new Employment() { Id = 6, ProjectId = 2, RoleId = 1, UserId = 1},
                new Employment() { Id = 7, ProjectId = 2, RoleId = 2, UserId = 2},
                new Employment() { Id = 8, ProjectId = 2, RoleId = 3, UserId = 3},
                new Employment() { Id = 9, ProjectId = 2, RoleId = 4, UserId = 6},
                new Employment() { Id = 10, ProjectId = 3, RoleId = 1, UserId = 5}
            };
    }
}
