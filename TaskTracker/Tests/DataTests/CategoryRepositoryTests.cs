﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Data;
using Data.Entities;
using Data.Repositories;

namespace Tests.DataTests
{
    public class CategoryRepositoryTests
    {
        [Fact]
        public async Task GetAll_ReturnsAllEntities()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);
            var expected = AllCategories;

            //act
            var actual = repo.GetAll() as IEnumerable<Category>;

            //assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetByIdAsync_ExistingId_ReturnsEntity(int id)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);
            var expected = AllCategories.FirstOrDefault(e => e.Id == id);

            //act
            var actual = await repo.GetByIdAsync(id);

            //assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Fact]
        public async Task AddAsync_Category_CountIncreasedByOne()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            var category = new Category { CategoryName ="123", UserId = 1};
            var expected = repo.GetAll().Count() + 1;

            //act
            await repo.AddAsync(category);
            await context.SaveChangesAsync();
            var actual = repo.GetAll().Count();

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task Update_ChangedEntity_EntityUpdated()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            var expected = new Category { Id = 1, CategoryName = "NewName", UserId = 2};

            //act
            repo.Update(expected);
            context.SaveChanges();
            var actual = await repo.GetByIdAsync(expected.Id);


            //assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Fact]
        public async Task Delete_ExistingEntity_EntityDeleted()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            var category = repo.GetAll().FirstOrDefault();
            var expected = repo.GetAll().Count() - 1;

            //act
            repo.Delete(category);
            await context.SaveChangesAsync();
            var actual = repo.GetAll().Count();

            //assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task DeleteByIdAsync_ValidId_EntityDeleted(int id)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            var expected = repo.GetAll().Count() - 1;

            //act
            await repo.DeleteByIdAsync(id);
            await context.SaveChangesAsync();
            var actual = repo.GetAll().Count();

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task GetAllWithDetails_ReturnsAllWithDetails()
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            var expected = AllCategories;

            //act
            var actual = repo.GetAllWithDetails();

            //assert
            Assert.Equal(expected, actual, Comparer);
            Assert.Equal(ExpectedUsers, actual.Select(e => e.User).AsEnumerable(), new UserProfileEqualityComparer());
            Assert.DoesNotContain(null, actual.Select(e => e.Projects).ToArray());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetByIdWithDetailsAsync_ValidId_ReturnsEntityWithDetails(int id)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            var expected = AllCategories.Where(e => e.Id == id).FirstOrDefault();
            expected.User = ExpectedUsers.Where(e => e.Id == expected.UserId).FirstOrDefault();

            //act
            var actual = await repo.GetByIdWithDetailsAsync(id);

            //assert
            Assert.Equal(expected, actual, Comparer);
            Assert.Equal(expected.User, actual.User, new UserProfileEqualityComparer());
            Assert.NotNull(actual.Projects);
        }

        [Theory]
        [InlineData(1, true)]
        [InlineData(11, false)]
        public async Task ExistsAsync_ReturnsCorrectValue(int id, bool expected)
        {
            //arrange
            await using var context = new TrackerContext(TestHelper.GetUnitTestDbOptions());
            var repo = new CategoryRepository(context);

            //act
            bool actual = await repo.ExistsAsync(id);

            //assert
            Assert.Equal(expected, actual);
        }

        private CategoryEqualityComparer Comparer => new CategoryEqualityComparer();

        private IEnumerable<Category> AllCategories =>
            new[]
            {
                new Category { Id = 1, UserId = 1, CategoryName = "Empire"},
                new Category { Id = 2, UserId = 4, CategoryName = "Council"},
                new Category { Id = 3, UserId = 5, CategoryName = "Space battles"}
            };

        private IEnumerable<UserProfile> ExpectedUsers =>
            new[]
            {
                new UserProfile { UserName = "Jar Jar", Email = "mesamail@gmail.com", UserId = "1", Id = 1},
                new UserProfile { UserName = "Kenobus", Email = "hello@there.com", UserId = "4", Id = 4 },
                new UserProfile { UserName = "Grievous", Email = "general@kenobus.com", UserId = "5", Id = 5 }
            };
    }
}
