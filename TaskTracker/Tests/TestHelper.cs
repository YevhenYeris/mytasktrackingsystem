﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Entities;
using Business;
using AutoMapper;

namespace Tests
{
    internal static class TestHelper
    {
        public static DbContextOptions<TrackerContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<TrackerContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new TrackerContext(options))
            {
                SeedData(context);
            }
            return options;
        }

        public static void SeedData(TrackerContext context)
        {
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Jar Jar", Email = "mesamail@gmail.com", UserId = "1", Id = 1 });
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Sheev Palpatine", Email = "do@it.com", UserId = "2" , Id = 2 });
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Young Skywalker", Email = "its@unfair.com", UserId = "3", Id = 3});
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Kenobus", Email = "hello@there.com", UserId = "4", Id = 4 });
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Grievous", Email = "general@kenobus.com", UserId = "5", Id = 5 });
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Clone1", Email = "cln1@kenobi.com", UserId = "6", Id = 6 });
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Clone2", Email = "cln2@kenobi.com", UserId = "7", Id = 7 });
            context.Set<UserProfile>().Add(new UserProfile { UserName = "Clone3", Email = "cln3@kenobi.com", UserId = "8", Id = 8 });

            context.Set<Role>().Add(new Role { Id = 1, Color = "#ffffff", RoleName = "Manager" });
            context.Set<Role>().Add(new Role { Id = 2, Color = "#ffffff", RoleName = "Co-manager" });
            context.Set<Role>().Add(new Role { Id = 3, Color = "#ffffff", RoleName = "Contributor" });
            context.Set<Role>().Add(new Role { Id = 4, Color = "#ffffff", RoleName = "Guest" });

            context.Set<Category>().Add(new Category { Id = 1, UserId = 1, CategoryName = "Empire"});
            context.Set<Category>().Add(new Category { Id = 2, UserId = 4, CategoryName = "Council"});
            context.Set<Category>().Add(new Category { Id = 3, UserId = 5, CategoryName = "Space battles"});

            context.Set<Project>().Add(new Project { Id = 1, CategoryId = 2, Color = "#ffffff", ProjectName = "Develop defence", StartTime = new DateTime(2021, 5, 4), DueTime = new DateTime(2022, 3, 4)});
            context.Set<Project>().Add(new Project { Id = 2, CategoryId = 1, Color = "#ffffff", ProjectName = "Execute order 66", StartTime = new DateTime(2021, 3, 20), DueTime = new DateTime(2021, 4, 20) });
            context.Set<Project>().Add(new Project { Id = 3, CategoryId = 3, Color = "#ffffff", ProjectName = "Battle", StartTime = new DateTime(2020, 12, 7)});

            context.Set<Employment>().Add(new Employment() { Id = 1, ProjectId = 1, RoleId = 1, UserId = 4});
            context.Set<Employment>().Add(new Employment() { Id = 2, ProjectId = 1, RoleId = 2, UserId = 3});
            context.Set<Employment>().Add(new Employment() { Id = 3, ProjectId = 1, RoleId = 3, UserId = 6});
            context.Set<Employment>().Add(new Employment() { Id = 4, ProjectId = 1, RoleId = 3, UserId = 7});
            context.Set<Employment>().Add(new Employment() { Id = 5, ProjectId = 1, RoleId = 3, UserId = 8});
            context.Set<Employment>().Add(new Employment() { Id = 6, ProjectId = 2, RoleId = 1, UserId = 1});
            context.Set<Employment>().Add(new Employment() { Id = 7, ProjectId = 2, RoleId = 2, UserId = 2});
            context.Set<Employment>().Add(new Employment() { Id = 8, ProjectId = 2, RoleId = 3, UserId = 3});
            context.Set<Employment>().Add(new Employment() { Id = 9, ProjectId = 2, RoleId = 4, UserId = 6});
            context.Set<Employment>().Add(new Employment() { Id = 10, ProjectId = 3, RoleId = 1, UserId = 5});
            context.Set<Employment>().Add(new Employment() { Id = 11, ProjectId = 3, RoleId = 4, UserId = 2});
            context.Set<Employment>().Add(new Employment() { Id = 12, ProjectId = 3, RoleId = 3, UserId = 3});
            context.Set<Employment>().Add(new Employment() { Id = 13, ProjectId = 3, RoleId = 3, UserId = 4});

            context.Set<ProjectTaskStatus>().Add(new ProjectTaskStatus() { Id = 1, StatusName = "Set Up"});
            context.Set<ProjectTaskStatus>().Add(new ProjectTaskStatus() { Id = 2, StatusName = "Started"});
            context.Set<ProjectTaskStatus>().Add(new ProjectTaskStatus() { Id = 3, StatusName = "In development"});
            context.Set<ProjectTaskStatus>().Add(new ProjectTaskStatus() { Id = 4, StatusName = "Stuck"});
            context.Set<ProjectTaskStatus>().Add(new ProjectTaskStatus() { Id = 5, StatusName = "Finished"});
            context.Set<ProjectTaskStatus>().Add(new ProjectTaskStatus() { Id = 6, StatusName = "Closed"});

            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 1, ProjectId = 1, TaskName = "Raise troops", TaskStatusId = 3});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 2, ProjectId = 1, TaskName = "Gather commanders", TaskStatusId = 2});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 3, ProjectId = 1, TaskName = "Set up the plan", TaskStatusId = 1});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 4, ProjectId = 2, TaskName = "Send the order", TaskStatusId = 5});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 5, ProjectId = 2, TaskName = "Kill the jedi", TaskStatusId = 4});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 6, ProjectId = 3, TaskName = "Set up the shields", TaskStatusId = 1});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 7, ProjectId = 3, TaskName = "Stop the jedi", TaskStatusId = 6});
            context.Set<ProjectTask>().Add(new ProjectTask() { Id = 8, ProjectId = 3, TaskName = "Abandon the ship", TaskStatusId = 3});

            context.Set<Assignment>().Add(new Assignment() { Id = 1, TaskId = 1, EmploymentId = 3});
            context.Set<Assignment>().Add(new Assignment() { Id = 2, TaskId = 1, EmploymentId = 4});
            context.Set<Assignment>().Add(new Assignment() { Id = 3, TaskId = 1, EmploymentId = 5});
            context.Set<Assignment>().Add(new Assignment() { Id = 4, TaskId = 2, EmploymentId = 1});
            context.Set<Assignment>().Add(new Assignment() { Id = 5, TaskId = 4, EmploymentId = 6});
            context.Set<Assignment>().Add(new Assignment() { Id = 6, TaskId = 5, EmploymentId = 7});
            context.Set<Assignment>().Add(new Assignment() { Id = 7, TaskId = 5, EmploymentId = 8});
            context.Set<Assignment>().Add(new Assignment() { Id = 8, TaskId = 5, EmploymentId = 9});
            context.Set<Assignment>().Add(new Assignment() { Id = 9, TaskId = 7, EmploymentId = 10});
            context.Set<Assignment>().Add(new Assignment() { Id = 10, TaskId = 8, EmploymentId = 5});
            context.Set<Assignment>().Add(new Assignment() { Id = 11, TaskId = 8, EmploymentId = 2});
            context.Set<Assignment>().Add(new Assignment() { Id = 12, TaskId = 8, EmploymentId = 3});
            context.Set<Assignment>().Add(new Assignment() { Id = 13, TaskId = 8, EmploymentId = 4});

            context.Set<TaskUpdate>().Add(new TaskUpdate() { Id = 1, TaskId = 1, EmploymentId = 4, UpdateTime = new DateTime()});
            context.Set<TaskUpdate>().Add(new TaskUpdate() { Id = 2, TaskId = 3, EmploymentId = 7, UpdateTime = new DateTime()});
            context.Set<TaskUpdate>().Add(new TaskUpdate() { Id = 3, TaskId = 5, EmploymentId = 11, UpdateTime = new DateTime()});
            context.Set<TaskUpdate>().Add(new TaskUpdate() { Id = 4, TaskId = 6, EmploymentId = 3, UpdateTime = new DateTime()});

            context.SaveChanges();
        }

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}
