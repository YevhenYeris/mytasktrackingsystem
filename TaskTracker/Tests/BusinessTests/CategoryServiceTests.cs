﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using Data.Repositories;
using Business;
using Business.Models;
using Business.Services;
using Xunit;
using Moq;

namespace Tests.BusinessTests
{
    public class CategoryServiceTests
    {
        [Fact]
        public void GetAll_ReturnsAll()
        {
            //arrange
            var mockedUoW = new Mock<IUnitOfWork>();
            mockedUoW.Setup(m => m.CategoryRepository.GetAllWithDetails()).Returns(AllCategories.AsQueryable());

            var service = new CategoryService(mockedUoW.Object, TestHelper.CreateMapperProfile());

            //act
            var actual = service.GetAll().OrderBy(e => e.Id).ToList();

            //assert
            for (int i = 0; i < AllCategories.Count(); ++i)
            {
                Assert.Equal(AllCategories[i].CategoryName, actual[i].CategoryName);
                Assert.Equal(AllCategories[i].Id, actual[i].Id);
                Assert.Equal(AllCategories[i].Projects.Select(e => e.Id), actual[i].ProjectsIds);
            }
        }

        private List<Category> AllCategories =>
        new List<Category>
        {
            new Category { Id = 1, UserId = 1, CategoryName = "Empire", Projects = new List<Project> { new Project { Id = 1 }, new Project { Id = 2 } } },
            new Category { Id = 2, UserId = 4, CategoryName = "Council", Projects = new List<Project> { new Project { Id = 3 }, new Project { Id = 4 } } },
            new Category { Id = 3, UserId = 5, CategoryName = "Space battles", Projects = new List<Project> { new Project { Id = 5 }, new Project { Id = 6 } } }
        };
    }
}
