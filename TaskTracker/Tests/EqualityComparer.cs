﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Tests
{
    internal class CategoryEqualityComparer : IEqualityComparer<Category>
    {
        public bool Equals([AllowNull] Category x, [AllowNull] Category y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.CategoryName == y.CategoryName
                && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Category obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class AssignmentEqualityComparer : IEqualityComparer<Assignment>
    {
        public bool Equals([AllowNull] Assignment x, [AllowNull] Assignment y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Description == y.Description
                && x.EmploymentId == y.EmploymentId
                && x.TaskId == y.TaskId;
        }

        public int GetHashCode([DisallowNull] Assignment obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class EmploymentEqualityComparer : IEqualityComparer<Employment>
    {
        public bool Equals([AllowNull] Employment x, [AllowNull] Employment y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.ProjectId == y.ProjectId
                && x.RoleId == y.RoleId
                && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Employment obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class UserProfileEqualityComparer : IEqualityComparer<UserProfile>
    {
        public bool Equals([AllowNull] UserProfile x, [AllowNull] UserProfile y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.UserId == y.UserId
                && x.Email == y.Email
                && x.UserId == y.UserId
                && x.UserName == y.UserName;

        }

        public int GetHashCode([DisallowNull] UserProfile obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ProjectEqualityComparer : IEqualityComparer<Project>
    {
        public bool Equals([AllowNull] Project x, [AllowNull] Project y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.CategoryId == y.CategoryId
                && x.Color == y.Color
                && x.Description == y.Description
                && x.DueTime == y.DueTime
                && x.FinishTime == y.FinishTime
                && x.ProjectName == y.ProjectName
                && x.StartTime == y.StartTime;
        }

        public int GetHashCode([DisallowNull] Project obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class RoleEqualityComparer : IEqualityComparer<Role>
    {
        public bool Equals([AllowNull] Role x, [AllowNull] Role y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Color == y.Color
                && x.RoleName == y.RoleName;
        }

        public int GetHashCode([DisallowNull] Role obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ProjectTaskEqualityComparer : IEqualityComparer<ProjectTask>
    {
        public bool Equals([AllowNull] ProjectTask x, [AllowNull] ProjectTask y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Description == y.Description
                && x.ProjectId == y.ProjectId
                && x.TaskName == y.TaskName
                && x.TaskStatusId == y.TaskStatusId;
        }

        public int GetHashCode([DisallowNull] ProjectTask obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ProjectTaskStatusEqualityComparer : IEqualityComparer<ProjectTaskStatus>
    {
        public bool Equals([AllowNull] ProjectTaskStatus x, [AllowNull] ProjectTaskStatus y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.StatusName == y.StatusName;
        }

        public int GetHashCode([DisallowNull] ProjectTaskStatus obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class TaskUpdateEqualityComparer : IEqualityComparer<TaskUpdate>
    {
        public bool Equals([AllowNull] TaskUpdate x, [AllowNull] TaskUpdate y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Description == y.Description
                && x.EmploymentId == y.EmploymentId
                && x.TaskId == y.TaskId
                && x.UpdateTime == y.UpdateTime;
        }

        public int GetHashCode([DisallowNull] TaskUpdate obj)
        {
            return obj.GetHashCode();
        }
    }
}
