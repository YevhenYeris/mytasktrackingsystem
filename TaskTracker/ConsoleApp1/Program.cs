﻿using System;
using Microsoft.EntityFrameworkCore;
using TT.Data;
using TT.Data.Entities;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (TrackerContext context = new TrackerContext())
            {
                User user = new User();
                UserProfile userProfile = new UserProfile() { UserId = user.Id};

                Category category = new Category();
                Project project = new Project() { CategoryId = category.Id };

                Role role = new Role();
                Employment employment = new Employment() { UserId = user.Id, ProjectId = project.Id, RoleId = role.Id};

                Task task = new Task() { ProjectId = project.Id};
                Assignment assignment = new Assignment() { EmploymentId = employment.Id, TaskId = task.Id };

                context.Set<User>().Add(user);
                context.Set<UserProfile>().Add(userProfile);
                context.Set<Category>().Add(category);
                context.Set<Project>().Add(project);
                context.Set<Role>().Add(role);
                context.Set<Employment>().Add(employment);
                context.Set<Task>().Add(task);
                context.Set<Assignment>().Add(assignment);
                
                
                context.SaveChanges();
            }    
        }
    }
}
