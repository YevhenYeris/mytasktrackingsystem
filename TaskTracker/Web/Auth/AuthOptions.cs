﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Auth
{
    public class AuthOptions
    {
        public const string ISSUER = "TrackerAuthServer";
        public const string AUDIENCE = "TrackerAuthClient";
        const string KEY = "tasktrackerkey.verysecret!";
        public const int LIFETIME = 5;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
