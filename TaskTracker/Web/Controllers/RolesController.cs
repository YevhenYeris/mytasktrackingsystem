﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _service;

        public RolesController(IRoleService service)
        {
            _service = service;
        }

        // GET: api/<RolesController>
        [HttpGet]
        public ActionResult<IEnumerable<RoleModel>> Get()
        {
            var models = _service.GetAll();

            return Ok(models);
        }

        // GET api/<RolesController>/5
        [HttpGet("{roleId}")]
        [NotFoundFilter]
        public async Task<ActionResult<RoleModel>> Get(int roleId)
        {
            var model = await _service.GetByIdAsync(roleId);

            return Ok(model);
        }

        // POST api/<RolesController>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<RoleModel>> Post([FromBody] RoleModel model)
        {
            int roleId = await _service.AddAsync(model);
            var added = await _service.GetByIdAsync(roleId);

            return Ok(added);
        }

        // PUT api/<RolesController>/5
        [HttpPut("{roleId}")]
        [Authorize(Roles = "admin")]
        [NotFoundFilter]
        public async Task<ActionResult<RoleModel>> Put(int roleId, [FromBody] RoleModel model)
        {
            if (roleId != model.Id) return BadRequest();

            await _service.UpdateAsync(model);
            var updated = await _service.GetByIdAsync(roleId);

            return Ok(updated);
        }

        // DELETE api/<RolesController>/5
        [HttpDelete("{roleId}")]
        [Authorize(Roles = "admin")]
        [BusinessExceptionFilter]
        public async Task<ActionResult> Delete(int roleId)
        {
            StatusCodeResult result = await _service.DeleteByIdAsync(roleId) ? NoContent() : NotFound();

            return result;
        }
    }
}
