﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Business.Filters;

namespace Web.Controllers
{
    /// <summary>
    /// A controller class implementing action methods
    /// for performing operations related to the <c>Category</c> entity
    /// </summary>
    /// <remarks>
    /// <para>
    /// Controller's route: "api/Categories"
    /// </para>
    /// </remarks>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoriesController : ControllerBase
    {
        /// <value>
        /// The <c>_service</c> private field represents a service
        /// implementing <c>ICategoryService</c> interface
        /// </value>
        private readonly ICategoryService _service;
        /// <value>
        /// The <c>_projectService</c> private field represents a service
        /// implementing <c>IProjectService</c> interface
        /// </value>
        private readonly IProjectService _projectService;

        /// <summary>
        /// Controller's constructor
        /// </summary>
        /// <param name="service">
        /// <c>ICategoryService</c> parameter
        /// </param>
        /// <param name="projectService">
        /// T<c>IProjectService</c> parameter
        /// </param>
        public CategoriesController(ICategoryService service, IProjectService projectService)
        {
            _service = service;
            _projectService = projectService;
        }

        /// <summary>
        /// Returns a collection of <c>CategoryModel</c> objects
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories
        /// </remarks>
        /// <returns>IEnumerable of categories</returns>
        /// <response code="200">If all requested items are 
        /// found</response>
        /// <response code="404">If user is not in "admin" role</response>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult<IEnumerable<CategoryModel>> Get()
        {
            var models = _service.GetAll();
             
            return Ok(models);
        }

        /// <summary>
        /// Returns a <c>CategoryModel</c> with the passed Id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories/1
        /// </remarks>
        /// <returns>The requested <c>CategoryModel</c></returns>
        /// <response code="200">If the requested item is
        /// found</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the
        ///requested category</response>
        ///<response code="404">If the requested category
        ///does not exist</response>
        [HttpGet("{categoryId}")]
        [ServiceFilter(typeof(FindCategoryFilter))]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        public async Task<ActionResult<CategoryModel>> Get(int categoryId)
        {
            var models = await _service.GetByIdAsync(categoryId);

            return Ok(models);
        }

        /// <summary>
        /// Adds a category to the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Post /api/Categories
        /// </remarks>
        /// <returns>The added <c>CategoryModel</c></returns>
        /// <response code="200">If the item is successfully
        /// added</response>
        /// <response code="400">If the passed item is not
        /// valid</response>
        /// <response code="401">If user is not in 
        /// "admin" or "user" role</response>
        [HttpPost]
        [Authorize(Roles = "admin,user")]
        public async Task<ActionResult<CategoryModel>> Post([FromBody] CategoryModel model)
        {
            int categoryId = await _service.AddAsync(model);
            var added = await _service.GetByIdAsync(categoryId);

            return Ok(added);
        }

        /// <summary>
        /// Updates an existing category
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Put /api/Categories/1
        /// </remarks>
        /// <returns>The updated <c>CategoryModel</c></returns>
        /// <response code="200">If the item is successfully
        /// updated</response>
        /// <response code="400">If the passed item is not
        /// valid or the Ids passed in the body and the path
        /// do not match</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpPut("{categoryId}")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult<CategoryModel>> Put(int categoryId, [FromBody] CategoryModel model)
        {
            if (categoryId != model.Id) return BadRequest();

            await _service.UpdateAsync(model);
            var updated = await _service.GetByIdAsync(categoryId);

            return Ok(updated);
        }

        /// <summary>
        /// Deletes an existing category
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Delete /api/Categories/1
        /// </remarks>
        /// <response code="204">If the item is successfully
        /// deleted</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpDelete("{categoryId}")]
        [BusinessExceptionFilter]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult> Delete(int categoryId)
        {
            StatusCodeResult result = (await _service.DeleteByIdAsync(categoryId)) ? NoContent() : NotFound();

            return result;
        }

        /// <summary>
        /// Returns the projects of the given category
        /// </summary>
        /// <returns>
        /// <c>IEnumerable</c> of <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories/1/projects
        /// </remarks>
        /// <response code="200">If all requested items are 
        /// found</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and the query do not match</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpGet("{categoryId}/projects")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetProjects(int categoryId, [FromQuery] ProjectFilterModel filter)
        {
            if (filter.CategoryId != 0 && categoryId != filter.CategoryId) return BadRequest(); 
            
            filter.CategoryId = categoryId;
            var projects = await _projectService.GetByFilterAsync(filter);

            return Ok(projects);
        }

        /// <summary>
        /// Returns the project with the given Id
        /// </summary>
        /// <returns>
        /// <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories/1/projects/1
        /// </remarks>
        /// <response code="200">If the requested item is 
        /// found</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and the query do not match or the project does not
        /// belongs to the category</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpGet("{categoryId}/projects/{projectId}")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        [ServiceFilter(typeof(ProjectBelongsToCategoryFilter))]
        public async Task<ActionResult<ProjectModel>> GetProject(int categoryId, int projectId)
        {
            var project = await _projectService.GetByIdAsync(projectId);

            return Ok(project);
        }

        /// <summary>
        /// Adds a project to the category
        /// </summary>
        /// <returns>
        /// The added <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Post /api/Categories/1/projects
        /// </remarks>
        /// <response code="200">If the item is successfully
        /// added</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and the body do not match</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpPost("{categoryId}/projects")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult<ProjectModel>> PostProject(int categoryId, [FromBody] ProjectModel model)
        {
            if (categoryId != model.CategoryId) return BadRequest();

            int addedId = await _projectService.AddAsync(model);
            var added = await _projectService.GetByIdAsync(addedId);

            return Ok(added);
        }

        /// <summary>
        /// Updates a project of the category
        /// </summary>
        /// <returns>
        /// The updated <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Put /api/Categories/1/projects/1
        /// </remarks>
        /// <response code="200">If the item is successfully
        /// updated</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and the dody do not match or the project does not
        /// belong to the category</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpPut("{categoryId}/projects/{projectId}")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        [ServiceFilter(typeof(ProjectBelongsToCategoryFilter))]
        public async Task<ActionResult<ProjectModel>> PutProject(int categoryId, int projectId, [FromBody] ProjectModel model)
        {
            if (categoryId != model.CategoryId || projectId != model.Id) return BadRequest();

            await _projectService.UpdateAsync(model);
            var updated = await _projectService.GetByIdAsync(projectId);

            return Ok(updated);
        }


        /// <summary>
        /// Deletes a project related to the category
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Delete /api/Categories/1/projects/1
        /// </remarks>
        /// <response code="204">If the item is
        /// successfully deleted</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the project does not belong to
        /// the category</response>
        /// <response code="404">If the category with the passed Id
        /// or the project are not found</response>
        [HttpDelete("{categoryId}/projects/{projectId}")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        [ServiceFilter(typeof(ProjectBelongsToCategoryFilter))]
        public async Task<ActionResult> DeleteProject(int categoryId, int projectId)
        {
            if ((await _projectService.GetByIdAsync(projectId))?.CategoryId != categoryId) return BadRequest();

            StatusCodeResult result = await _projectService.DeleteByIdAsync(projectId) ? NoContent() : NotFound();

            return result;
        }

        /// <summary>
        /// Returns the finished projects related to the category
        /// </summary>
        /// <returns>
        /// <c>IEnumerable</c> of <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories/1/projects/finished
        /// </remarks>
        /// <response code="200">If all the requested items
        /// are found</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and in the do not match</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpGet("{categoryId}/projects/finished")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetFinishedProjects(int categoryId, [FromQuery] ProjectFilterModel filter)
        {
            if (filter.CategoryId != 0 && categoryId != filter.CategoryId) return BadRequest();

            filter.CategoryId = categoryId;
            var projects = await _projectService.GetFinishedAsync(filter);

            return Ok(projects);
        }

        /// <summary>
        /// Returns the unfinished projects related to the category
        /// </summary>
        /// <returns>
        /// <c>IEnumerable</c> of <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories/1/projects/unfinished
        /// </remarks>
        /// <response code="200">If all the requested items
        /// are found</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and in the do not match</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpGet("{categoryId}/projects/unfinished")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetUnfinishedProjects(int categoryId, [FromQuery] ProjectFilterModel filter)
        {
            if (categoryId != 0 && categoryId != filter.CategoryId) return BadRequest();

            filter.CategoryId = categoryId;
            var projects = await _projectService.GetUnfinishedAsync(filter);

            return Ok(projects);
        }

        /// <summary>
        /// Returns the overdue projects related to the category
        /// </summary>
        /// <returns>
        /// <c>IEnumerable</c> of <c>ProjectModel</c>
        /// </returns>
        /// <remarks>
        /// Sample request:
        /// Get /api/Categories/1/projects/overdue
        /// </remarks>
        /// <response code="200">If all the requested items
        /// are found</response>
        /// <response code="401">If user is not in 
        /// "admin" role and does not own the category</response>
        /// <response code="400">If the categoryId passed in the path
        /// and in the do not match</response>
        /// <response code="404">If the category with the passed Id
        /// is not found</response>
        [HttpGet("{categoryId}/projects/overdue")]
        [TypeFilter(typeof(OwnsCategoryFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindCategoryFilter))]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetOverdueProjects(int categoryId, [FromQuery] ProjectFilterModel filter)
        {
            if (categoryId != 0 && categoryId != filter.CategoryId) return BadRequest();

            filter.CategoryId = categoryId;
            var projects = await _projectService.GetOverdueAsync(filter);

            return Ok(projects);
        }
    }
}
