﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Filters;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _service;
        private readonly ITaskService _taskService;

        public ProjectsController(IProjectService service, ITaskService taskService)
        {
            _service = service;
            _taskService = taskService;
        }

        // GET: api/<ProjectsController>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> Get([FromQuery] ProjectFilterModel filter)
        {
            var models = await _service.GetByFilterAsync(filter);

            return Ok(models);
        }

        // GET api/<ProjectsController>/5
        [HttpGet("{projectId}")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<ProjectModel>> Get(int projectId)
        {
            var model = await _service.GetByIdAsync(projectId);

            return Ok(model);
        }

        [HttpGet("{projectId}/completion")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<int>> GetCompletion(int projectId)
        {
            int completion = await _service.GetCompletionAsync(projectId);

            return Ok(completion);
        }

        // POST api/<ProjectsController>
        [HttpPost]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult<ProjectModel>> Post([FromBody] ProjectModel model)
        {
            int projectId = await _service.AddAsync(model);
            var added = await _service.GetByIdAsync(projectId);

            return Ok(added);
        }

        // PUT api/<ProjectsController>/5
        [HttpPut("{projectId}")]
        [TypeFilter(typeof(OwnsProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<ProjectModel>> Put(int projectId, [FromBody] ProjectModel model)
        {
            if (projectId != model.Id) return BadRequest();

            await _service.UpdateAsync(model);
            var updated = await _service.GetByIdAsync(projectId);

            return Ok(updated);
        }

        // DELETE api/<ProjectsController>/5
        [HttpDelete("{projectId}")]
        [TypeFilter(typeof(OwnsProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        [BusinessExceptionFilter]
        public async Task<ActionResult> Delete(int projectId)
        {
            StatusCodeResult result = await _service.DeleteByIdAsync(projectId) ? NoContent() : NotFound();

            return result;
        }

        [HttpGet("finished")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetFinished([FromQuery] ProjectFilterModel filter)
        {
            var result = await _service.GetFinishedAsync(filter);

            return Ok(result);
        }

        [HttpGet("unfinished")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetUnFinished([FromQuery] ProjectFilterModel filter)
        {
            var result = await _service.GetUnfinishedAsync(filter);

            return Ok(result);
        }

        [HttpGet("overdue")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetOverdue([FromQuery] ProjectFilterModel filter)
        {
            var result = await _service.GetOverdueAsync(filter);

            return Ok(result);
        }

        [HttpPost("{projectId}/users")]
        [TypeFilter(typeof(OwnsProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult> EmployUser(int projectId, [FromBody] EmployUserModel model)
        {
            if (projectId != model.ProjectId) return BadRequest();

            await _service.EmployUser(model);

            return Ok();
        }

        [HttpDelete("{projectId}/users")]
        [TypeFilter(typeof(OwnsProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult> UnemployUser(int projectId, [FromBody] UnemployUserModel model)
        {
            if (projectId != model.ProjectId) return BadRequest();

            await _service.UnemployUser(model);

            return Ok();
        }

        [HttpGet("{projectId}/users")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetUsers(int projectId)
        {
            var result = await _service.GetUsersAsync(projectId);

            return Ok(result);
        }

        [HttpGet("{projectId}/usersRoles")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<IEnumerable<UserRoleModel>>> GetUsersWithRoles(int projectId)
        {
            var result = await _service.GetUsersWithRolesAsync(projectId);

            return Ok(result);
        }

        [HttpGet("{projectId}/tasks")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public ActionResult<IEnumerable<TaskModel>> GetTasks(int projectId, [FromQuery] TaskFilterModel filter)
        {
            if (filter.ProjectId != 0 && filter.ProjectId != projectId) return BadRequest();

            filter.ProjectId = projectId;
            var result = _taskService.GetByFilter(filter);

            return Ok(result);
        }

        [HttpGet("{projectId}/tasks/{taskId}")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        [ServiceFilter(typeof(TaskBelongsToProjectFilter))]
        public async Task<ActionResult<TaskModel>> GetTask(int projectId, int taskId)
        {
            var result = await _taskService.GetByIdAsync(taskId);

            return Ok(result);
        }

        [HttpPost("{projectId}/tasks")]
        [TypeFilter(typeof(OwnsProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<TaskModel>> PostTask(int projectId, [FromBody] TaskModel model)
        {
            if (model.ProjectId != projectId) return BadRequest();

            int addedId = await _taskService.AddAsync(model);
            var added = await _taskService.GetByIdAsync(addedId);

            return Ok(added);
        }

        [HttpPut("{projectId}/tasks/{taskId}")]
        [TypeFilter(typeof(OwnsProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        [ServiceFilter(typeof(TaskBelongsToProjectFilter))]
        public async Task<ActionResult<TaskModel>> PutTask(int projectId, int taskId, [FromBody] TaskModel model)
        {
            if (taskId != model.Id || model.ProjectId != projectId) return BadRequest();

            await _taskService.UpdateAsync(model);
            var updated = await _taskService.GetByIdAsync(taskId);

            return Ok(updated);
        }

        [HttpDelete("{projectId}/tasks/{taskId}")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        [ServiceFilter(typeof(TaskBelongsToProjectFilter))]
        public async Task<ActionResult> DeleteTask(int projectId, int taskId)
        {
            if ((await _taskService.GetByIdAsync(taskId))?.ProjectId != projectId) return BadRequest();

            StatusCodeResult result = await _taskService.DeleteByIdAsync(taskId) ? NoContent() : NotFound(); 

            return result;
        }

        [HttpGet("{projectId}/tasks/unassigned")]
        [TypeFilter(typeof(SeesProjectFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindProjectFilter))]
        public ActionResult<IEnumerable<TaskModel>> GetUnassignedTasks(int projectId, [FromQuery] TaskFilterModel filter)
        {
            if (projectId != filter.ProjectId) return BadRequest();

            filter.ProjectId = projectId;
            var result = _taskService.GetUnassigned(filter);

            return Ok(result);
        }
    }
}
