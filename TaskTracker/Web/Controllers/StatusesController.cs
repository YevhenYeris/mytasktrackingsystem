﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StatusesController : ControllerBase
    {
        private readonly IStatusService _service;

        public StatusesController(IStatusService service)
        {
            _service = service;
        }

        // GET: api/<StatusesController>
        [HttpGet]
        public ActionResult<IEnumerable<StatusModel>> Get()
        {
            var models = _service.GetAll();

            return Ok(models);
        }

        // GET api/<StatusesController>/5
        [HttpGet("{statusId}")]
        [NotFoundFilter]
        public async Task<ActionResult<StatusModel>> Get(int statusId)
        {
            var model = await _service.GetByIdAsync(statusId);

            return Ok(model);
        }

        // POST api/<StatusesController>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<StatusModel>> Post([FromBody] StatusModel model)
        {
            int statusId = await _service.AddAsync(model);
            var added = await _service.GetByIdAsync(statusId);

            return Ok(added);
        }

        // PUT api/<StatusesController>/5
        [HttpPut("{statusId}")]
        [Authorize(Roles = "admin")]
        [NotFoundFilter]
        public async Task<ActionResult<StatusModel>> Put(int statusId, [FromBody] StatusModel model)
        {
            if (statusId! != model.Id) return BadRequest();

            await _service.UpdateAsync(model);
            var updated = await _service.GetByIdAsync(statusId);

            return Ok(updated);
        }

        // DELETE api/<StatusesController>/5
        [HttpDelete("{statusId}")]
        [Authorize(Roles = "admin")]
        [BusinessExceptionFilter]
        public async Task<ActionResult> Delete(int statusId)
        {
            StatusCodeResult result = await _service.DeleteByIdAsync(statusId) ? NoContent() : NotFound();

            return result;
        }
    }
}
