﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Business.Interfaces;
using Business.Models;
using Business.Filters;
using Web.ViewModels;
using Web.Auth;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly ICategoryService _categoryService;
        private readonly IProjectService _projectService;
        private readonly ITaskService _taskService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public UsersController(IUserService service,
                               SignInManager<IdentityUser> signInManager,
                               UserManager<IdentityUser> userManager,
                               ICategoryService categoryService,
                               IProjectService projectService,
                               ITaskService taskService)
        {
            _service = service;
            _signInManager = signInManager;
            _userManager = userManager;
            _categoryService = categoryService;
            _projectService = projectService;
            _taskService = taskService;
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<ActionResult> Register([FromBody] CreateUserModel model)
        {
            await _service.AddAsync(model);
            await _service.SignInAsync(new LogInModel { UserName = model.UserName, Password = model.Password });

            return Ok();
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult> LogIn([FromBody] LogInModel model)
        {
            var claims = await _service.SignInAsync(model);
            if (claims == null) return BadRequest(new { errorText = "Invalid username or password." });

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: claims.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt
            };

            return Ok(response);
        }

        [HttpDelete]
        [Authorize]
        public async Task<ActionResult> LogOut()
        {
            await _service.LogOutAsync();

            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult<IEnumerable<ProfileModel>> Get()
        {
            var result = _service.GetAll();

            return Ok(result);
        }

        [HttpGet("{userId}")]
        [TypeFilter(typeof(OwnsProfileFilter), Arguments = new object[] { "admin, user" })]
        [NotFoundFilter]
        public async Task<ActionResult<IEnumerable<ProfileModel>>> Get(int userId)
        {
            var result = await _service.GetByIdAsync(userId);

            return Ok(result);
        }

        [HttpGet("{userId}/categories")]
        [TypeFilter(typeof(OwnsProfileFilter), Arguments = new object[] { "admin" })]
        public async Task<ActionResult<IEnumerable<CategoryModel>>> GetCategories(int userId)
        {
            var result = await _service.GetCategories(userId);

            return Ok(result);
        }

        [HttpDelete("{userId}")]
        [TypeFilter(typeof(OwnsProfileFilter), Arguments = new object[] { "admin" })]
        [BusinessExceptionFilter]
        public async Task<ActionResult> Delete(int userId)
        {
            StatusCodeResult result = await _service.DeleteByIdAsync(userId) ? NoContent() : NotFound();

            return result;
        }

        [HttpGet("{userId}/projects")]
        [TypeFilter(typeof(OwnsProfileFilter), Arguments = new object[] { "admin" })]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetProjects(int userId, [FromQuery] ProjectFilterModel filter)
        {
            if (filter.UserId != 0 && userId != filter.UserId) return BadRequest();

            filter.UserId = userId;
            var result = await _projectService.GetByFilterAsync(filter);

            return Ok(result);
        }

        [HttpGet("{userId}/projects/{projectId}")]
        [TypeFilter(typeof(OwnsProfileFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(ProjectBelongsToUserFilter))]
        [ServiceFilter(typeof(FindProjectFilter))]
        public async Task<ActionResult<ProjectModel>> GetProject(int userId, int projectId)
        {
            var result = await _projectService.GetByIdAsync(projectId);

            return Ok(result);
        }

        [HttpGet("{userId}/projects/{projectId}/tasks")]
        [TypeFilter(typeof(OwnsProfileFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(ProjectBelongsToUserFilter))]
        [ServiceFilter(typeof(FindProjectFilter))]
        public ActionResult<IEnumerable<TaskModel>> GetTasks(int userId, int projectId, [FromQuery] TaskFilterModel filter)
        {
            if (filter.ProjectId != 0 && projectId != filter.ProjectId) return BadRequest();

            filter.ProjectId = projectId;
            var result = _taskService.GetByFilter(filter);

            return Ok(result);
        }

        [HttpGet("isSigned")]
        public ActionResult<bool> IsSigned()
        {
            return Ok(HttpContext.User.Identity.IsAuthenticated);
        }
    }
}
