﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Filters;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _service;

        public TasksController(ITaskService service)
        {
            _service = service;
        }

        // GET: api/<TasksController>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult<IEnumerable<TaskModel>> Get([FromQuery] TaskFilterModel filter)
        {
            var models = _service.GetByFilter(filter);

            return Ok(models);
        }

        [HttpGet("unassigned")]
        [Authorize(Roles = "admin")]
        public ActionResult<IEnumerable<TaskModel>> GetUnassigned([FromQuery] TaskFilterModel filter)
        {
            var models = _service.GetUnassigned(filter);

            return Ok(models);
        }

        [HttpGet("{taskId}/roles")]
        [TypeFilter(typeof(SeesTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public ActionResult<IEnumerable<RoleModel>> GetRoles(int taskId)
        {
            var models = _service.GetRoles(taskId);

            return Ok(models);
        }

        // GET api/<TasksController>/5
        [HttpGet("{taskId}")]
        [TypeFilter(typeof(SeesTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public async Task<ActionResult<TaskModel>> Get(int taskId)
        {
            var model = await _service.GetByIdAsync(taskId);

            return Ok(model);
        }

        // POST api/<TasksController>
        [HttpPost]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult<TaskModel>> Post([FromBody] TaskModel model)
        {
            int taskId = await _service.AddAsync(model);
            var added = await _service.GetByIdAsync(taskId);

            return Ok(added);
        }

        [HttpPost("{taskId}/users")]
        [TypeFilter(typeof(OwnsTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public async Task<ActionResult> AssignUser(int taskId, [FromBody] AssignUserModel model)
        {
            if (taskId != model.TaskId) return BadRequest();

            await _service.AssignUserAsync(model);

            return Ok();
        }

        [HttpDelete("{taskId}/users")]
        [TypeFilter(typeof(OwnsTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public async Task<ActionResult> DismissUser(int taskId, [FromBody] DismissUserModel model)
        {
            if (taskId != model.TaskId) return BadRequest();

            await _service.DismissUserAsync(model);

            return Ok();
        }

        [HttpPost("{taskId}/updates")]
        [TypeFilter(typeof(SeesTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public async Task<ActionResult<UpdateModel>> PostUpdate([FromBody] UpdateModel update)
        {
            var model = await _service.SendUpdateAsync(update);

            return Ok(model);
        }

        // PUT api/<TasksController>/5
        [HttpPut("{taskId}")]
        [TypeFilter(typeof(OwnsTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public async Task<ActionResult<TaskModel>> Put(int taskId, [FromBody] TaskModel model)
        {
            if (taskId != model.Id) return BadRequest();

            await _service.UpdateAsync(model);
            var updated = await _service.GetByIdAsync(taskId);

            return Ok(updated);
        }

        // DELETE api/<TasksController>/5
        [HttpDelete("{taskId}")]
        [TypeFilter(typeof(OwnsTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        [BusinessExceptionFilter]
        public async Task<ActionResult> Delete(int taskId)
        {
            StatusCodeResult result = await _service.DeleteByIdAsync(taskId) ? NoContent() : NotFound();

            return result;
        }

        [HttpGet("{taskId}/updates")]
        [TypeFilter(typeof(SeesTaskFilter), Arguments = new object[] { "admin" })]
        [ServiceFilter(typeof(FindTaskFilter))]
        public ActionResult<IEnumerable<UpdateModel>> GetUpdates(int taskId)
        {
            var models = _service.GetUpdates(taskId);

            return Ok(models);
        }
    }
}
