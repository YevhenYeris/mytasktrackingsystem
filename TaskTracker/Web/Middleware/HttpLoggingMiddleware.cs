﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Middleware
{
    public class HttpLoggingMiddleware
    {
        private RequestDelegate _next;
        private ILogger<HttpLoggingMiddleware> _logger;

        public HttpLoggingMiddleware(RequestDelegate next, ILogger<HttpLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            await LogRequest(context);
            await LogResponse(context);
        }

        private async Task LogRequest(HttpContext context)
        {
            _logger.LogInformation($"Request: " +
                                   $"{context.Request.Method} " +
                                   $"{context.Request.Path} " +
                                   $"{context.Request.QueryString} " +
                                   $"Body: {await ReadRequestBodyAsync(context)} ");

        }

        private async Task<string> ReadRequestBodyAsync(HttpContext context)
        {
            string res = "";

            context.Request.EnableBuffering();
            context.Request.Body.Seek(0, SeekOrigin.Begin);

            var reader = new StreamReader(context.Request.Body);
            
            res = await reader.ReadToEndAsync();
            context.Request.Body.Seek(0, SeekOrigin.Begin);


            return res;
        }

        private async Task<string> ReadResponseBodyAsync(HttpContext context)
        {
            Stream originalBody = context.Response.Body;
            string responseBody = "";

            using (var memStream = new MemoryStream())
            {
                try
                {
                    context.Response.Body = memStream;

                    await _next(context);

                    memStream.Position = 0;
                    responseBody = await new StreamReader(memStream).ReadToEndAsync();

                    memStream.Position = 0;
                    await memStream.CopyToAsync(originalBody);
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex.ToString());
                    throw;
                }
                finally
                {
                    context.Response.Body = originalBody;
                }
            }

            return string.Join(string.Empty, responseBody.Where(e => !char.IsWhiteSpace(e)));
        }

        private async Task LogResponse(HttpContext context)
        {
            _logger.LogInformation($"Response: " +
                                   $"{context.Request.Method} " +
                                   $"{context.Request.Path} " +
                                   $"{context.Request.QueryString} " +
                                   $"Code: {context.Response.StatusCode} " +
                                   $"Body: {await ReadResponseBodyAsync(context)}");            
        }
    }
            
    public static class HttpLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseHttpLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpLoggingMiddleware>();
        }
    }
}
