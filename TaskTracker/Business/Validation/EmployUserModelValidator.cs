﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Data.Interfaces;
using FluentValidation;

namespace Business.Validation
{
    public class EmployUserModelValidator : AbstractValidator<EmployUserModel>
    {
        public EmployUserModelValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => m.RoleId)
                .Must(id => unitOfWork.RoleRepository.ExistsAsync(id).Result)
                .WithMessage("Role with the given id does not exist");

            this.RuleFor(m => m.ProjectId)
                .Must(id => unitOfWork.ProjectRepository.ExistsAsync(id).Result)
                .WithMessage("Project with the given id does not exist");

            this.RuleFor(m => m.UserEmail)
                .Must(email => unitOfWork.ProfileRepository.GetAll().Where(e => e.Email == email).Select(e => e.Id).Any())
                .WithMessage("User with the given id does not exist");

            this.RuleFor(m => new { m.ProjectId, m.UserEmail })
                .Must(m => !unitOfWork.EmploymentRepository.GetAll()
                                                .Where(e => e.ProjectId == m.ProjectId && e.UserId == unitOfWork.ProfileRepository.GetAll().Where(e => e.Email == m.UserEmail).Select(e => e.Id).FirstOrDefault())
                                                .Any())
                .WithMessage("The User is already assigned to the Project");
        }
    }
}
