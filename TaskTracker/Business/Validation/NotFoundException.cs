﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Validation
{
    public class NotFoundException : Exception
    {
        public override string Message => "Entity does not exist";
    }
}
