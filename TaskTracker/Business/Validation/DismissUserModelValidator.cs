﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Business.Models;
using FluentValidation;

namespace Business.Validation
{
    public class DismissUserModelValidator : AbstractValidator<DismissUserModel>
    {
        public DismissUserModelValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => m.TaskId)
                .Must(id => unitOfWork.TaskRepository.ExistsAsync(id).Result);

            this.RuleFor(m => m.UserId)
                .Must(id => unitOfWork.ProfileRepository.ExistsAsync(id).Result)
                .WithMessage("User with the given id does not exist");

            this.RuleFor(m => new { m.TaskId, m.UserId })
                .Must(m => unitOfWork.AssignmentRepository.GetAllWithDetails()
                                                .Where(e => e.TaskId == m.TaskId && e.Employment.UserId == m.UserId)
                                                .Any())
                .WithMessage("The User is not assigned to the task");
        }
    }
}
