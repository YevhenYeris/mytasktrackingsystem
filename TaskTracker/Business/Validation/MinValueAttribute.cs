﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;

namespace Business.Validation
{
    public class MinValueAttribute : ValidationAttribute
    {
        private readonly int _value;

        public MinValueAttribute(int value)
        {
            _value = value;
        }

        public override bool IsValid(object value)
        {
            return (int)value >= _value;
        }

        public override string FormatErrorMessage(string name)
        {
            return $"The field {name} must be a value greater than {_value}";
        }
    }
}
