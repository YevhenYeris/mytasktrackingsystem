﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Business.Models;
using FluentValidation;

namespace Business.Validation
{
    public class UpdateModelValidator : AbstractValidator<UpdateModel>
    {
        public UpdateModelValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => new { m.TaskId, m.UserId })
                .Cascade(CascadeMode.Stop)
                .Must(id => unitOfWork.TaskRepository.ExistsAsync(id.TaskId).Result)
                .WithMessage("Task with the given id does not exist")
                .Must(m => unitOfWork.TaskRepository.ExistsAsync(m.TaskId).Result &&
                           unitOfWork.EmploymentRepository.GetAll()
                                                          .Where(e =>
                                                                 e.UserId == m.UserId &&
                                                                 e.ProjectId == unitOfWork.TaskRepository
                                                                 .GetByIdAsync(m.TaskId).Result.ProjectId)
                                                                 .Any() &&
                           unitOfWork.TaskRepository.GetAllWithDetails().Where(e => e.Project.Category.UserId == m.UserId).Any())
                .WithMessage("The User is not assigned to the Project");

            this.RuleFor(m => m.UserId)
                .Must(id => unitOfWork.ProfileRepository.ExistsAsync(id).Result)
                .WithMessage("User with the given id does not exist");
        }
    }
}
