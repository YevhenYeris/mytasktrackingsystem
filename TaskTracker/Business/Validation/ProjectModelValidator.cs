﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Business.Models;
using FluentValidation;

namespace Business.Validation
{
    public class ProjectModelValidator : AbstractValidator<ProjectModel>
    {
        public ProjectModelValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => m.CategoryId)
                .Must(id => unitOfWork.CategoryRepository.ExistsAsync(id).Result)
                .WithMessage("Category with the given id does not exist");
        }
    }
}
