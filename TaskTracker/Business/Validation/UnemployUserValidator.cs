﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Business.Models;
using FluentValidation;

namespace Business.Validation
{
    public class UnemployUserValidator : AbstractValidator<UnemployUserModel>
    {
        public UnemployUserValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => m.ProjectId)
                .Must(id => unitOfWork.ProjectRepository.ExistsAsync(id).Result)
                .WithMessage("Project with the given id does not exist");

            this.RuleFor(m => m.UserId)
                .Must(id => unitOfWork.ProfileRepository.ExistsAsync(id).Result)
                .WithMessage("User with the given id does not exist");

            this.RuleFor(m => new { m.ProjectId, m.UserId })
                .Must(m => unitOfWork.EmploymentRepository.GetAll()
                                                .Where(e => e.ProjectId == m.ProjectId && e.UserId == m.UserId)
                                                .Any())
                .WithMessage("The User is not assigned to the Project");
        }
    }
}
