﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Business.Models;
using FluentValidation;

namespace Business.Validation
{
    public class CategoryModelValidator : AbstractValidator<CategoryModel>
    {
        public CategoryModelValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => m.UserId)
                .Must(id => unitOfWork.ProfileRepository.ExistsAsync(id).Result)
                .WithMessage("User with the given id does not exist");
        }
    }
}
