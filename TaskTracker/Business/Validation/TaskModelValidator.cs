﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Business.Models;
using FluentValidation;

namespace Business.Validation
{
    public class TaskModelValidator : AbstractValidator<TaskModel>
    {
        public TaskModelValidator(IUnitOfWork unitOfWork)
        {
            this.RuleFor(m => m.ProjectId)
                .Must(id => unitOfWork.ProjectRepository.ExistsAsync(id).Result)
                .WithMessage("Project with the given id does not exist");

            this.RuleFor(m => m.TaskStatusId)
                .Must(id => unitOfWork.TaskStatusRepository.ExistsAsync(id).Result)
                .WithMessage("TaskStatus with the given id does not exist");
        }
    }
}
