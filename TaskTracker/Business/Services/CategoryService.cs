﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Data.Entities;
using Business;
using Business.Validation;
using Business.Interfaces;
using Business.Models;
using System.Threading.Tasks;
using AutoMapper;
using Business.Exceptions;

namespace Business.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICategoryRepository _categoryRepository;
        private readonly Mapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _categoryRepository = unitOfWork.CategoryRepository;
            _mapper = mapper;
        }

        public async Task<int> AddAsync(CategoryModel model)
        {
            var entity = _mapper.Map<Category>(model);

            await _categoryRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<bool> DeleteByIdAsync(int modelId)
        {
            if (!await _categoryRepository.ExistsAsync(modelId)) return false;
            if ((await _categoryRepository.GetByIdWithDetailsAsync(modelId)).Projects.Any())
                throw new CannotDeleteException("Category cannot be deleted: related projects exist");

            await _categoryRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveChangesAsync();

            return !await _categoryRepository.ExistsAsync(modelId);
        }

        public IEnumerable<CategoryModel> GetAll()
        {
            var entities = _categoryRepository.GetAllWithDetails();
            return _mapper.Map<IEnumerable<CategoryModel>>(entities);
        }

        public async Task<CategoryModel> GetByIdAsync(int id)
        {
            var entity = await _categoryRepository.GetByIdAsync(id);

            return _mapper.Map<CategoryModel>(entity);
        }

        public async Task UpdateAsync(CategoryModel model)
        {
            var entity = await _categoryRepository.GetByIdAsync(model.Id);
            if (entity != null) await UpdateAsync(entity, model);
        }

        private async Task UpdateAsync(Category entity, CategoryModel model)
        {
            entity.CategoryName = model.CategoryName;
            entity.UserId = model.UserId;

            _categoryRepository.Update(entity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
