﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Data.Entities;
using Business;
using Business.Validation;
using Business.Interfaces;
using Business.Models;
using System.Threading.Tasks;
using AutoMapper;

namespace Business.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectRepository _projectRepository;
        private readonly Mapper _mapper;

        public ProjectService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = unitOfWork.ProjectRepository;
            _mapper = mapper;
        }

        public async Task<int> AddAsync(ProjectModel model)
        {
            var entity = _mapper.Map<Project>(model);

            await _projectRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task EmployUser(EmployUserModel model)
        {
            var userId = _unitOfWork.ProfileRepository.GetAll()
                                                      .Where(e => e.Email == model.UserEmail)
                                                      .Select(e => e.Id)
                                                      .FirstOrDefault();

            var employment = new Employment { ProjectId = model.ProjectId, RoleId = model.RoleId, UserId = userId,
                RecieveNotifications = true };

            await _unitOfWork.EmploymentRepository.AddAsync(employment);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UnemployUser(UnemployUserModel model)
        {
            var employments = _unitOfWork.EmploymentRepository.GetAll()
                                                .Where(e => e.ProjectId == model.ProjectId && e.UserId == model.UserId)
                                                .Select(e => e.Id);

            foreach (var item in employments)
            {
                await _unitOfWork.EmploymentRepository.DeleteByIdAsync(item);
            }
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DeleteByIdAsync(int modelId)
        {
            if (!await _projectRepository.ExistsAsync(modelId)) return false;

            await _projectRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveChangesAsync();

            return !await _projectRepository.ExistsAsync(modelId);
        }

        public IEnumerable<ProjectModel> GetAll()
        {
            var entities = _projectRepository.GetAll();

            return _mapper.Map<IEnumerable<ProjectModel>>(entities);
        }

        private async Task<IEnumerable<ProjectModel>> ApplyFilterAsync(IEnumerable<Project> entities, ProjectFilterModel filter)
        {
            entities = filter?.CategoryId != 0 ? entities.Where(e => e.CategoryId == filter.CategoryId) : entities;
            entities = filter?.ManagerId != 0 ? entities.Where(e => e.Category.UserId == filter.ManagerId) : entities;
            entities = filter?.UserId != 0 ? entities.Where(e => e.Employments.Select(e => e.UserId).Contains(filter.UserId) ||
                                                                 e.Category.UserId == filter.UserId) : entities;

            entities = filter?.ProjectName != string.Empty && filter?.ProjectName != null ? entities.Where(e => e.ProjectName == filter.ProjectName) : entities;

            var models = _mapper.Map<IEnumerable<ProjectModel>>(entities);
            if (filter.WithCompletion)
                foreach (var item in models)
                    item.Completion = await GetCompletionAsync(item.Id);

            return models;
        }

        public async Task<IEnumerable<ProjectModel>> GetByFilterAsync(ProjectFilterModel filter)
        {
            if (filter == null) return GetAll();

            var models = await ApplyFilterAsync(_projectRepository.GetAllWithDetails(), filter);
            return models;
        }

        public async Task<ProjectModel> GetByIdAsync(int id)
        {
            var entity = await _projectRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<ProjectModel>(entity);
        }

        public async Task<IEnumerable<ProjectModel>> GetFinishedAsync(ProjectFilterModel filter = null)
        {
            var entities = _projectRepository.GetAllWithDetails().Where(e => e.FinishTime != DateTime.MinValue);
            var models = await ApplyFilterAsync(entities, filter);

            return models;
        }

        public async Task<IEnumerable<ProjectModel>> GetOverdueAsync(ProjectFilterModel filter = null)
        {
            var entities = _projectRepository.GetAllWithDetails().Where(e => e.DueTime <= DateTime.Now);
            var models = await ApplyFilterAsync(entities, filter);

            return models;
        }

        public async Task<IEnumerable<ProjectModel>> GetUnfinishedAsync(ProjectFilterModel filter = null)
        {
            var entities = _projectRepository.GetAllWithDetails().Where(e => e.FinishTime <= DateTime.Now);
            var models = await ApplyFilterAsync(entities, filter);

            return models;
        }

        public async Task UpdateAsync(ProjectModel model)
        {
            var entity = await _projectRepository.GetByIdAsync(model.Id);
            if (entity != null) await UpdateAsync(entity, model);
        }

        private async Task UpdateAsync(Project entity, ProjectModel model)
        {
            entity.CategoryId = model.CategoryId;
            entity.Color = model.Color;
            entity.Description = model.Description;
            entity.DueTime = model.DueTime;
            entity.ProjectName = model.ProjectName;
            entity.StartTime = model.StartTime;

            _projectRepository.Update(entity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<int> GetCompletionAsync(int projectId)
        {
            var project = await _projectRepository.GetByIdWithDetailsAsync(projectId);

            int all = project.Tasks.Count();
            int finished = project.Tasks.Where(e => e.IsFinished).Count();

            int completion = all == 0 ? 100 : (int)((double)finished / all * 100);

            return completion;
        }

        public async Task<IEnumerable<UserModel>> GetUsersAsync(int projectId)
        {
            var project = await _projectRepository.GetByIdWithDetailsAsync(projectId);
            var usersIds = project.Employments.Select(e => e.UserId);

            var users = usersIds.Select(async e => await _unitOfWork.ProfileRepository.GetByIdAsync(e)).ToList();

            return _mapper.Map<IEnumerable<UserModel>>(users.Select(e => e.Result));
        }

        public async Task<IEnumerable<TaskModel>> GetTasksAsync(int projectId)
        {
            var project = await _projectRepository.GetByIdWithDetailsAsync(projectId);
            var tasks = project.Tasks;

            return _mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        public async Task<IEnumerable<UserRoleModel>> GetUsersWithRolesAsync(int projectId)
        {
            var project = await _projectRepository.GetByIdWithDetailsAsync(projectId);
            var usersRoles = project.Employments.Select(async e => new UserRoleModel { RoleId = e.RoleId,
                                                                               RoleName = (await _unitOfWork.RoleRepository.GetByIdAsync(e.RoleId)).RoleName,
                                                                               UserId = e.UserId,
                                                                               UserName = (await _unitOfWork.ProfileRepository.GetByIdAsync(e.UserId)).UserName,
                                                                               RoleColor = (await _unitOfWork.RoleRepository.GetByIdAsync(e.RoleId)).Color
            });

            return usersRoles.Select(e => e.Result);
        }
    }
}
