﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Business.Services
{
    public class RoleInitializer
    {
        public static async Task InitializeAsync(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            string adminEmail = "admin@gmail.com";
            string adminPassword = "admin123";

            string userEmail = "user@gmail.com";
            string userPassword = "user123";

            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (await roleManager.FindByNameAsync("user") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("user"));
            }
            if (await userManager.FindByEmailAsync(adminEmail) == null)
            {
                IdentityUser admin = new IdentityUser { Email = adminEmail, UserName = adminEmail };
                IdentityResult result = await userManager.CreateAsync(admin, adminPassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");
                }
            }
            if (await userManager.FindByEmailAsync(userEmail) == null)
            {
                IdentityUser user = new IdentityUser { Email = userEmail, UserName = userEmail };
                IdentityResult result = await userManager.CreateAsync(user, userPassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "user");
                }
            }
            if (!await userManager.IsInRoleAsync(await userManager.FindByEmailAsync(adminEmail), "admin"))
            {
                await userManager.AddToRoleAsync(await userManager.FindByEmailAsync(adminEmail), "admin");
            }
            if (!await userManager.IsInRoleAsync(await userManager.FindByEmailAsync(userEmail), "user"))
            {
                await userManager.AddToRoleAsync(await userManager.FindByEmailAsync(userEmail), "user");
            }
        }
    }
}