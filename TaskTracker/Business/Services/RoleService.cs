﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using Data.Entities;
using AutoMapper;

namespace Business.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleRepository _roleRepository;
        private readonly Mapper _mapper;

        public RoleService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _roleRepository = unitOfWork.RoleRepository;
            _mapper = mapper;
        }

        public async Task<int> AddAsync(RoleModel model)
        {
            var entity = _mapper.Map<Role>(model);

            await _roleRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<bool> DeleteByIdAsync(int modelId)
        {
            if (!await _roleRepository.ExistsAsync(modelId)) return false;
            await _roleRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveChangesAsync();

            return !await _roleRepository.ExistsAsync(modelId);
        }

        public IEnumerable<RoleModel> GetAll()
        {
            var entities = _roleRepository.GetAllWithDetails();

            return _mapper.Map<IEnumerable<RoleModel>>(entities);
        }

        public async Task<RoleModel> GetByIdAsync(int id)
        {
            var entity = await _roleRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<RoleModel>(entity);
        }

        public IEnumerable<RoleModel> GetByTaskId(int id)
        {
            var entities = from assignment in _unitOfWork.AssignmentRepository.GetAllWithDetails()
                           where assignment.TaskId == id
                           select assignment.Employment.RoleId;

            return _mapper.Map<IEnumerable<RoleModel>>(entities);
        }

        public async Task UpdateAsync(RoleModel model)
        {
            var entity = await _roleRepository.GetByIdAsync(model.Id);
            if (entity != null) await UpdateAsync(entity, model);
        }

        public async Task UpdateAsync(Role entity, RoleModel model)
        {
            entity.Color = model.Color;
            entity.RoleName = model.RoleName;

            _roleRepository.Update(entity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
