﻿using System;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Text;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using Data.Entities;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        public readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public readonly SignInManager<IdentityUser> _signInManager;
        private readonly Mapper _mapper;
        public UserService(IUnitOfWork unitOfWork,
                           UserManager<IdentityUser> userManager,
                           RoleManager<IdentityRole> roleManager,
                           SignInManager<IdentityUser> signInManager,
                           Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public async Task AddAsync(CreateUserModel model)
        {
            IdentityUser user = new IdentityUser { Email = model.Email, UserName = model.UserName };

            var creation = await _userManager.CreateAsync(user, model.Password);

            if (creation.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "user");

                ProfileModel profile = new ProfileModel
                {
                    UserId = (await _userManager.FindByEmailAsync(user.Email)).Id,
                    UserName = user.UserName,
                    Email = user.Email,
                    CategoriesIds = new int[] { },
                    ProjectsIds = new int[] { }
                };
                await AddAsync(profile);
            }
        }

        public async Task<int> AddAsync(ProfileModel model)
        {
            if (_unitOfWork.ProfileRepository.GetAll().Where(e => e.UserId == model.UserId).Any()) throw new ArgumentException();

            var entity = _mapper.Map<UserProfile>(model);
            await _unitOfWork.ProfileRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<bool> DeleteByIdAsync(int modelId)
        {
            if (!await _unitOfWork.ProfileRepository.ExistsAsync(modelId)) return false;
            var userProfile = await _unitOfWork.ProfileRepository.GetByIdWithDetailsAsync(modelId);
            
            await _unitOfWork.ProfileRepository.DeleteByIdAsync(modelId);

            await _userManager.DeleteAsync(await _userManager.FindByIdAsync(userProfile.UserId));

            await _unitOfWork.SaveChangesAsync();

            return !await _unitOfWork.ProfileRepository.ExistsAsync(modelId);
        }

        public IEnumerable<ProfileModel> GetAll()
        {
            IEnumerable<UserProfile> entities = _unitOfWork.ProfileRepository.GetAllWithDetails();

            return _mapper.Map<IEnumerable<ProfileModel>>(entities);
        }

        public IEnumerable<ProfileModel> GetByFilter(UserFilterModel filter)
        {
            throw new NotImplementedException();
        }

        public async Task<ProfileModel> GetByIdAsync(int id)
        {
            var entity = await _unitOfWork.ProfileRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<ProfileModel>(entity);
        }

        public async Task<IEnumerable<CategoryModel>> GetCategories(int id)
        {
            var user = await _unitOfWork.ProfileRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<IEnumerable<CategoryModel>>(user.Categories);
        }

        public IEnumerable<ProfileModel> GetUnemployed()
        {
            throw new NotImplementedException();
        }

        public async Task<ClaimsIdentity> SignInAsync(LogInModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            user = user == null ? await _userManager.FindByEmailAsync(model.UserName) : user;

            if (user == null) return null;

            var signRes = await _signInManager.PasswordSignInAsync(user, model.Password, true, false);
            if (!signRes.Succeeded) return null;
            var principal = await _signInManager.CreateUserPrincipalAsync(user);

            var profile = _unitOfWork.ProfileRepository.GetAll().Where(e => e.UserId == user.Id).FirstOrDefault();
            if (profile == null)
            {
                ProfileModel profileModel = new ProfileModel { UserName = user.UserName, UserId = user.Id, Email = user.Email };
                int profileId = await AddAsync(profileModel);
                profile = await _unitOfWork.ProfileRepository.GetByIdAsync(profileId);
            }

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultNameClaimType, profile.Id.ToString())
            };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }

        public async Task LogOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public Task UpdateAsync(ProfileModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GenerateTokenAsync(IdentityUser user)
        {
            return await _userManager.GenerateUserTokenAsync(user, "jwt", "access");
        }

        public async Task<UserModel> GetByEmail(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            return _mapper.Map<UserModel>(user);
        }
    }
}
