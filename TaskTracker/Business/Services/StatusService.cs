﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Interfaces;
using Data.Entities;
using Business.Interfaces;
using Business.Models;
using AutoMapper;
using System.Threading.Tasks;

namespace Business.Services
{
    public class StatusService : IStatusService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITaskStatusRepository _statusRepository;
        private readonly Mapper _mapper;

        public StatusService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _statusRepository = unitOfWork.TaskStatusRepository;
            _mapper = mapper;
        }

        public async Task<int> AddAsync(StatusModel model)
        {
            var entity = _mapper.Map<ProjectTaskStatus>(model);

            await _statusRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<bool> DeleteByIdAsync(int modelId)
        {
            if (!await _statusRepository.ExistsAsync(modelId)) return false;
            await _statusRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveChangesAsync();

            return !await _statusRepository.ExistsAsync(modelId);
        }

        public IEnumerable<StatusModel> GetAll()
        {
            var entities = _statusRepository.GetAllWithDetails();

            return _mapper.Map<IEnumerable<StatusModel>>(entities);
        }

        public async Task<StatusModel> GetByIdAsync(int id)
        {
            var entity = await _statusRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<StatusModel>(entity);
        }

        public async Task UpdateAsync(StatusModel model)
        {
            var entity = await _statusRepository.GetByIdAsync(model.Id);
            if (entity != null) await UpdateAsync(entity, model);
        }

        public async Task UpdateAsync(ProjectTaskStatus entity, StatusModel model)
        {
            entity.StatusName = model.StatusName;

            _statusRepository.Update(entity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
