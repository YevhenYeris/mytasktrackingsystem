﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using Data.Entities;
using AutoMapper;
using System.Threading.Tasks;

namespace Business.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITaskRepository _taskRepository;
        private readonly Mapper _mapper;

        public TaskService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _taskRepository = unitOfWork.TaskRepository;
            _mapper = mapper;
        }

        public async Task<int> AddAsync(TaskModel model)
        {
            if (model.TaskStatusId == _unitOfWork.TaskStatusRepository.GetAll()
                                                          .Where(e => e.StatusName == "Finished")
                                                          .FirstOrDefault().Id)
            {
                model.IsFinished = true;
            }
            else
            {
                model.IsFinished = false;
            }

            var entity = _mapper.Map<ProjectTask>(model);

            await _taskRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task AssignUserAsync(AssignUserModel model)
        {
            var task = await _taskRepository.GetByIdWithDetailsAsync(model.TaskId);

            var employment = _unitOfWork.EmploymentRepository.GetAllWithDetails()
                                        .Where(e => e.ProjectId == task.ProjectId && e.UserId == model.UserId).FirstOrDefault();

            var entity = new Assignment { TaskId = model.TaskId, EmploymentId = employment.Id, Description = model.Description };

            await _unitOfWork.AssignmentRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DismissUserAsync(DismissUserModel model)
        {
            var assignments = (await _taskRepository.GetByIdWithDetailsAsync(model.TaskId)).Assignments.Where(e => e.Employment.UserId == model.UserId);

            foreach (var assignment in assignments)
                await _unitOfWork.AssignmentRepository.DeleteByIdAsync(assignment.Id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DeleteByIdAsync(int modelId)
        {
            if (!await _taskRepository.ExistsAsync(modelId)) return false;
            await _taskRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveChangesAsync();

            return !await _taskRepository.ExistsAsync(modelId);
        }

        public IEnumerable<TaskModel> GetAll()
        {
            var entities = _taskRepository.GetAllWithDetails();

            return _mapper.Map<IEnumerable<TaskModel>>(entities);
        }

        public IEnumerable<ProjectTask> ApplyFilter(TaskFilterModel filter)
        {
            var entities = _taskRepository.GetAllWithDetails();

            entities = filter?.ProjectId != 0 ? entities.Where(e => e.ProjectId == filter.ProjectId) : entities;
            entities = filter?.StatusId != 0 ? entities.Where(e => e.TaskStatusId == filter.StatusId) : entities;
            entities = filter?.TaskName != string.Empty && filter?.TaskName != null ? entities.Where(e => e.TaskName == filter.TaskName) : entities;

            if (filter?.UserId != 0 && filter?.UserId != null)
            {
                var tasksIds = _unitOfWork.EmploymentRepository.GetAllWithDetails()
                                                               .Where(e => e.UserId == filter.UserId)
                                                               .SelectMany(e => e.Assignments
                                                               .Select(e => e.TaskId))
                                                               .Distinct()
                                                               .ToList();
                entities = entities.Where(e => tasksIds.Contains(e.Id));
            }

            return entities.AsEnumerable();
        }

        public IEnumerable<TaskModel> GetByFilter(TaskFilterModel filter)
        {
            if (filter == null) return GetAll();

            var entities = ApplyFilter(filter);
            return _mapper.Map<IEnumerable<TaskModel>>(entities);
        }

        public async Task<TaskModel> GetByIdAsync(int id)
        {
            var entity = await _taskRepository.GetByIdWithDetailsAsync(id);

            return _mapper.Map<TaskModel>(entity);
        }

        public IEnumerable<TaskModel> GetUnassigned(TaskFilterModel filter = null)
        {
            var entities = ApplyFilter(filter);
            entities = entities.Where(e => !e.Assignments.Any());

            return _mapper.Map<IEnumerable<TaskModel>>(entities);
        }

        public async Task<UpdateModel> SendUpdateAsync(UpdateModel model)
        {
            var entity = _mapper.Map<TaskUpdate>(model);
            var task = await _taskRepository.GetByIdAsync(model.TaskId);
            entity.Employment = null;
            entity.EmploymentId = _unitOfWork.EmploymentRepository.GetAll()
                                                                  .Where(e => e.UserId == model.UserId && e.ProjectId == task.ProjectId)
                                                                  .FirstOrDefault().Id;
            entity.UpdateTime = DateTime.Now;

            await _unitOfWork.TaskUpdateRepository.AddAsync(entity);
            await _unitOfWork.SaveChangesAsync();

            return _mapper.Map<UpdateModel>(entity);
        }

        public async Task UpdateAsync(TaskModel model)
        {
            var entity = await _taskRepository.GetByIdAsync(model.Id);

            if (entity != null)
            {
                if (model.TaskStatusId == _unitOfWork.TaskStatusRepository.GetAll()
                                                          .Where(e => e.StatusName == "Finished")
                                                          .FirstOrDefault().Id)
                {
                    model.IsFinished = true;
                }
                else
                {
                    model.IsFinished = false;
                }

                await UpdateAsync(entity, model);
            }
        }

        private async Task UpdateAsync(ProjectTask entity, TaskModel model)
        {
            entity.Description = model.Description;
            entity.DueTime = model.DueTime;
            entity.FinishTime = model.FinishTime;
            entity.IsFinished = model.IsFinished;
            entity.ProjectId = model.ProjectId;
            entity.StartTime = model.StartTime;
            entity.TaskName = model.TaskName;
            entity.TaskStatusId = model.TaskStatusId;

            _taskRepository.Update(entity);
            await _unitOfWork.SaveChangesAsync();
        }

        public IEnumerable<RoleModel> GetRoles(int id)
        {
            var rolesIds = _unitOfWork.AssignmentRepository.GetAllWithDetails()
                                                           .Where(e => e.TaskId == id)
                                                           .Select(e => e.Employment.RoleId)
                                                           .Distinct();

            List<Role> roles = new List<Role>();
            foreach(var roleId in rolesIds)
            {
                roles.Add(_unitOfWork.RoleRepository.GetByIdAsync(roleId).Result);
            }

            return _mapper.Map<IEnumerable<RoleModel>>(roles);
        }

        public IEnumerable<UpdateModel> GetUpdates(int id)
        {
            var entities = _unitOfWork.TaskUpdateRepository.GetAllWithDetails();
            entities = entities.Where(e => e.TaskId == id);

            return _mapper.Map<IEnumerable<UpdateModel>>(entities);
        }
    }
}
