﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text;

namespace Business.Exceptions
{
    [Serializable]
    public class CannotDeleteException: Exception
    {
        public CannotDeleteException()
            :base()
        {
        }

        public CannotDeleteException(string message)
            :base(message)
        {
        }

        public CannotDeleteException(string message, Exception inner)
            :base(message, inner)
        {
        }

        public CannotDeleteException(SerializationInfo info,
            StreamingContext context)
        {
        }
    }
}
