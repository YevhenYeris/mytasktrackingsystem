﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Business.Validation;

namespace Business.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(5)]
        public string CategoryName { get; set; }

        [MinValue(1)]
        public int UserId { get; set; }

        public virtual ICollection<int> ProjectsIds { get; set; }
    }
}
