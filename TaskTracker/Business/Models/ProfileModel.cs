﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using Business.Validation;

namespace Business.Models
{
    public class ProfileModel
    {
        public int Id { get; set; }

        [Required]
        [MinValue(1)]
        public string UserId { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public ICollection<int> ProjectsIds { get; set; }

        public ICollection<int> CategoriesIds { get; set; }
    }
}
