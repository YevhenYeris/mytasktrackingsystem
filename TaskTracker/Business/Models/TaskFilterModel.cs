﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class TaskFilterModel
    {
        public int ProjectId { get; set; }

        public int UserId { get; set; }

        public int StatusId { get; set; }

        public string TaskName { get; set; }
    }
}
