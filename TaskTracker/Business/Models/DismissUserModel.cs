﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class DismissUserModel
    {
        public int UserId { get; set; }

        public int TaskId { get; set; }

        public bool Notify { get; set; }
    }
}
