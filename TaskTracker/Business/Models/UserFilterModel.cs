﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class UserFilterModel
    {
        public int ProjectId { get; set; }

        public int TaskId { get; set; }

        public int RoleId { get; set; }
    }
}
