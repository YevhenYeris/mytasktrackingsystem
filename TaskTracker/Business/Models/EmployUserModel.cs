﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class EmployUserModel
    {
        public int ProjectId { get; set; }

        public string UserEmail { get; set; }

        public int RoleId { get; set; }

        public bool Notify { get; set; }
    }
}
