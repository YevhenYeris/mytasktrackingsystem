﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using Business.Validation;

namespace Business.Models
{
    public class TaskModel
    {
        public int Id { get; set; }

        [Required]
        public string TaskName { get; set; }

        public string Description { get; set; }

        [Required]
        [MinValue(1)]
        public int ProjectId { get; set; }

        [Required]
        [MinValue(1)]
        public int TaskStatusId { get; set; }

        [Required]
        public bool IsFinished { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        public DateTime FinishTime { get; set; }

        [Required]
        public DateTime DueTime { get; set; }

        public ICollection<int> UserIds { get; set; }

        public ICollection<int> UpdatesIds { get; set; }
    }
}
