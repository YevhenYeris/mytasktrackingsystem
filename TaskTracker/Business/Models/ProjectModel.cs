﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using Business.Validation;

namespace Business.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }

        [Required]
        public string ProjectName { get; set; }

        [Required]
        public string Color { get; set; }

        public string Description { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DueTime { get; set; }

        [Required]
        [MinValue(1)]
        public int CategoryId { get; set; }

        public int? Completion { get; set; }

        public ICollection<int> UsersIds { get; set; }

        public ICollection<int> TasksIds { get; set; }
    }
}
