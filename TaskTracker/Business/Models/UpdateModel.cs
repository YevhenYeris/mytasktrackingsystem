﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using Business.Validation;

namespace Business.Models
{
    public class UpdateModel
    {
        public int Id { get; set; }

        [Required]
        [MinValue(1)]
        public int UserId { get; set; }

        [Required]
        [MinValue(1)]
        public int TaskId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime UpdateTime { get; set; }

        [Required]
        [MinLength(15)]
        public string Description { get; set; }
    }
}
