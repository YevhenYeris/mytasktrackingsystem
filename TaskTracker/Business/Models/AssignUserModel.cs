﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class AssignUserModel
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int TaskId { get; set; }

        public string Description { get; set; }

        public bool Notify { get; set; }
    }
}
