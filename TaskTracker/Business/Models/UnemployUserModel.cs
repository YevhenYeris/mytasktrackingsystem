﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class UnemployUserModel
    {
        public int ProjectId { get; set; }

        public int UserId { get; set; }

        public bool Notify { get; set; }
    }
}
