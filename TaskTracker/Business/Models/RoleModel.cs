﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class RoleModel
    {
        public int Id { get; set; }

        [Required]
        public string RoleName { get; set; }

        [Required]
        public string Color { get; set; }

        public ICollection<int> UsersIds { get; set; }
    }
}
