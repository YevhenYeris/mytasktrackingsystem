﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class StatusModel
    {
        public int Id { get; set; }

        [Required]
        public string StatusName { get; set; }

        public virtual ICollection<int> TasksIds { get; set; }
    }
}
