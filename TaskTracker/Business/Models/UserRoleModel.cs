﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class UserRoleModel
    {
        public int UserId { get; set; }

        public int RoleId { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

        public string RoleColor { get; set; }
    }
}
