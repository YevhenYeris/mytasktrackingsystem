﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class ProjectFilterModel
    {
        public int UserId { get; set; }

        public int ManagerId { get; set; }

        public int CategoryId { get; set; }

        public string ProjectName { get; set; }

        public bool WithCompletion { get; set; }
    }
}
