﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Data.Interfaces;
using Data.Entities;

namespace Business.Filters
{
    public abstract class AccessesObjectFilter: IAsyncAuthorizationFilter
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly string[] _roles = { };

        protected UserProfile UserProfile { get; set; }
        protected int ObjectId { get; set; }

        public AccessesObjectFilter(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager, string roles)
        {
            _ = unitOfWork ?? throw new ArgumentNullException();
            _ = userManager ?? throw new ArgumentNullException();
            _ = roles ?? throw new ArgumentNullException();

            roles = string.Concat(roles.Where(e => !char.IsWhiteSpace(e)));
            _roles = roles != string.Empty ? roles.Split(',') : _roles;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }
         
        public virtual async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var result = context.Result;
            context.Result = new UnauthorizedResult();

            //Check if authenticated
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                context.Result = new OkResult();
                return;
            };

            var user = await GetUserAsync(context);

            if (await CheckRolesAsync(user))
            {
                context.Result = new OkResult();
                return;
            }

            UserProfile = _unitOfWork.ProfileRepository.GetAllWithDetails().Where(e => e.UserId == user.Id).FirstOrDefault();
            if (UserProfile == null) return;

            SetObjectId(context);

            context.Result = result;
        }

        private async Task<IdentityUser> GetUserAsync(AuthorizationFilterContext context)
        {
            var principal = context.HttpContext.User;
            var user = await _userManager.GetUserAsync(principal);

            return user;
        }

        private async Task<bool> CheckRolesAsync(IdentityUser user)
        {
            foreach (var role in _roles)
            {
                if (await _userManager.IsInRoleAsync(user, role))
                    return true;
            }

            return false;
        }

        private void SetObjectId(AuthorizationFilterContext context)
        {
            int id;
            if (!int.TryParse(context.HttpContext.Request.Path.Value.Split('/')[3], out id)) return;

            ObjectId = id;
        }
    }
}
