﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Data.Interfaces;

namespace Business.Filters
{
    public class ProjectBelongsToUserFilter : IAsyncActionFilter
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectBelongsToUserFilter(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            object userId, projectId;
            if (!context.ActionArguments.TryGetValue("userId", out userId) ||
                !context.ActionArguments.TryGetValue("projectId", out projectId) ||
                !(projectId is int) ||
                !(userId is int))
            {
                await next(); return;
            }

            var user = await _unitOfWork.ProfileRepository.GetByIdWithDetailsAsync((int)userId);

            if (!(user?.Categories?.Where(e => e.Projects.Where(e => e.Id == (int)projectId).Any()).Any()).GetValueOrDefault())
            {
                context.Result = new BadRequestObjectResult("The given project does not belong to the given user");
            }

            await next();
        }
    }
}
