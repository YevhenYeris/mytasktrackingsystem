﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Identity;
using Data.Interfaces;

namespace Business.Filters
{
    public class OwnsProjectFilter : AccessesObjectFilter
    {
        public OwnsProjectFilter(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager, string roles = "")
            : base(unitOfWork, userManager, roles)
        {
        }

        public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            await base.OnAuthorizationAsync(context);

            if (context.Result is UnauthorizedObjectResult) return;
            if (context.Result is OkResult) { context.Result = null; return; };

            if (!UserProfile.Categories.Where(e => e.Projects.Where(e => e.Id == ObjectId).Any()).Any())
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
