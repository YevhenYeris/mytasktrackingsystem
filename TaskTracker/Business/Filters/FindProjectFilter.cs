﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Data.Interfaces;

namespace Business.Filters
{
    public class FindProjectFilter : IAsyncActionFilter
    {
        private readonly IUnitOfWork _unitOfWork;

        public FindProjectFilter(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            object id;
            if (!context.ActionArguments.TryGetValue("projectId", out id) || !(id is int)) { await next(); return; }

            if (!await _unitOfWork.ProjectRepository.ExistsAsync((int)id))
            {
                context.Result = new NotFoundObjectResult("Project with the given id does not exist");
                return;
            }

            await next();
        }
    }
}
