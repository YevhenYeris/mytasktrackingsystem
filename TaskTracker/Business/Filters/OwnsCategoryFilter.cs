﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Data.Interfaces;

namespace Business.Filters
{
    public class OwnsCategoryFilter : AccessesObjectFilter
    {
        public OwnsCategoryFilter(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager, string roles = "")
            :base(unitOfWork, userManager, roles)
        {
        }

        public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            await base.OnAuthorizationAsync(context);

            if (context.Result is UnauthorizedObjectResult) return;
            if (context.Result is OkResult)
            {
                context.Result = null;
                return;
            };

            if (!UserProfile.Categories.Where(e => e.Id == ObjectId).Any())
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
