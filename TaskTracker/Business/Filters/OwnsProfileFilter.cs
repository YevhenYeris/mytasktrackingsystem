﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Identity;
using Data.Interfaces;

namespace Business.Filters
{
    public class OwnsProfileFilter : AccessesObjectFilter
    {
        public OwnsProfileFilter(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager, string roles = "")
            : base(unitOfWork, userManager, roles)
        {
        }

        public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            await base.OnAuthorizationAsync(context);

            if (context.Result is UnauthorizedObjectResult) return;
            if (context.Result is OkResult) { context.Result = null; return; };

            if (UserProfile.Id != ObjectId)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
