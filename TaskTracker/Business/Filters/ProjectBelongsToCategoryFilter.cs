﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Data.Interfaces;

namespace Business.Filters
{
    public class ProjectBelongsToCategoryFilter : IAsyncActionFilter
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectBelongsToCategoryFilter(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            object categoryId, projectId;
            if (!context.ActionArguments.TryGetValue("categoryId", out categoryId) ||
                !context.ActionArguments.TryGetValue("projectId", out projectId)||
                !(projectId is int) ||
                !(categoryId is int))
            {
                await next(); return;
            }

            var category = await _unitOfWork.CategoryRepository.GetByIdWithDetailsAsync((int)categoryId);

            if (!(category?.Projects?.Where(e => e.Id == (int)projectId).Any()).GetValueOrDefault())
            {
                context.Result = new BadRequestObjectResult("The given project does not belong to the given category");
            }

            await next();
        }
    }
}
