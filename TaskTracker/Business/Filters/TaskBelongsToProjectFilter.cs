﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Data.Interfaces;

namespace Business.Filters
{
    public class TaskBelongsToProjectFilter : IAsyncActionFilter
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskBelongsToProjectFilter(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            object taskId, projectId;
            if (!context.ActionArguments.TryGetValue("taskId", out taskId) ||
                !context.ActionArguments.TryGetValue("projectId", out projectId) ||
                !(projectId is int) ||
                !(taskId is int))
            {
                await next(); return;
            }

            var project = await _unitOfWork.ProjectRepository.GetByIdWithDetailsAsync((int)projectId);

            if (!(project?.Tasks?.Where(e => e.Id == (int)taskId).Any()).GetValueOrDefault())
            {
                context.Result = new BadRequestObjectResult("The given task does not belong to the given project");
            }

            await next();
        }
    }
}
