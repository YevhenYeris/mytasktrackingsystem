﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Business.Exceptions;

namespace Business.Filters
{
    public class BusinessExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            if (exception.GetType() == typeof(CannotDeleteException))
            {
                context.Result = new ConflictObjectResult(exception.Message);
                return;   
            }
        }
    }
}
