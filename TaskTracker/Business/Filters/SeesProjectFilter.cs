﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Data.Interfaces;

namespace Business.Filters
{
    public class SeesProjectFilter : AccessesObjectFilter
    {
        public SeesProjectFilter(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager, string roles = "")
            : base(unitOfWork, userManager, roles)
        {
        }

        public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            await base.OnAuthorizationAsync(context);

            if (context.Result is UnauthorizedObjectResult) return;
            if (context.Result is OkResult) { context.Result = null; return; };

            var hasEmployments = UserProfile.Employments?.Where(e => e.ProjectId == ObjectId).Any();
            var hasProjects = UserProfile.Categories?.Where(e => e.Projects != null && e.Projects.Where(e => e.Id == ObjectId).Any()).Any();

            if ((hasEmployments.HasValue && !hasEmployments.Value) && (hasProjects.HasValue && !hasProjects.Value))
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
