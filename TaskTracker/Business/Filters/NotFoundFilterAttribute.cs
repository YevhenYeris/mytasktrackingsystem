﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Business.Filters
{
    public class NotFoundFilterAttribute : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            context.Result = (context.Result as OkObjectResult)?.Value == null ? new NotFoundResult() : context.Result;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}
