﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;

namespace Business.Interfaces
{
    public interface ICrudService<TModel> where TModel : class
    {
        IEnumerable<TModel> GetAll();

        Task<TModel> GetByIdAsync(int id);

        Task<int> AddAsync(TModel model);

        Task UpdateAsync(TModel model);

        Task<bool> DeleteByIdAsync(int modelId);

    }
}
