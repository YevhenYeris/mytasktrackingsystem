﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using Business.Models;

namespace Business.Interfaces
{
    public interface IProjectService : ICrudService<ProjectModel>
    {
        public Task<IEnumerable<ProjectModel>> GetByFilterAsync(ProjectFilterModel filter);

        public Task<IEnumerable<ProjectModel>> GetFinishedAsync(ProjectFilterModel filter = null);

        public Task<IEnumerable<ProjectModel>> GetUnfinishedAsync(ProjectFilterModel filter = null);

        public Task<IEnumerable<ProjectModel>> GetOverdueAsync(ProjectFilterModel filter = null);

        public Task EmployUser(EmployUserModel model);

        public Task UnemployUser(UnemployUserModel model);

        public Task<int> GetCompletionAsync(int projectId);

        public Task<IEnumerable<UserModel>> GetUsersAsync(int projectId);

        public Task<IEnumerable<UserRoleModel>> GetUsersWithRolesAsync(int projectId);
    }
}
