﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using Business.Models;

namespace Business.Interfaces
{
    public interface IRoleService : ICrudService<RoleModel>
    {
    }
}
