﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace Business.Interfaces
{
    public interface IUserService : ICrudService<ProfileModel>
    {
        public Task AddAsync(CreateUserModel model);

        public Task<ClaimsIdentity> SignInAsync(LogInModel model);

        public Task LogOutAsync();

        public IEnumerable<ProfileModel> GetByFilter(UserFilterModel filter);

        public Task<IEnumerable<CategoryModel>> GetCategories(int id);

        public IEnumerable<ProfileModel> GetUnemployed();

        public Task<UserModel> GetByEmail(string email);
    }
}
