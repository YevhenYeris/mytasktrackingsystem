﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using Business.Models;

namespace Business.Interfaces
{
    public interface ITaskService : ICrudService<TaskModel>
    {
        public IEnumerable<TaskModel> GetByFilter(TaskFilterModel filter);

        public IEnumerable<TaskModel> GetUnassigned(TaskFilterModel filter = null);

        public IEnumerable<RoleModel> GetRoles(int id);

        public IEnumerable<UpdateModel> GetUpdates(int id);

        public Task AssignUserAsync(AssignUserModel model);

        public Task DismissUserAsync(DismissUserModel model);

        public Task<UpdateModel> SendUpdateAsync(UpdateModel update);
    }
}
