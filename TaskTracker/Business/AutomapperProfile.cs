﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Data.Entities;
using Business.Models;
using Microsoft.AspNetCore.Identity;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Category, CategoryModel>()
                .ForMember(trg => trg.ProjectsIds, src => src.MapFrom(e => e.Projects.Select(e => e.Id)))
                .ReverseMap();

            CreateMap<Role, RoleModel>()
                .ForMember(trg => trg.UsersIds, src => src.MapFrom(e => e.Employments.Select(e => e.UserId)))
                .ReverseMap();

            CreateMap<ProjectTaskStatus, StatusModel>()
                .ForMember(trg => trg.TasksIds, src => src.MapFrom(e => e.Tasks.Select(e => e.Id)))
                .ReverseMap();

            CreateMap<Project, ProjectModel>()
                .ForMember(trg => trg.TasksIds, src => src.MapFrom(e => e.Tasks.Select(e => e.Id)))
                .ForMember(trg => trg.UsersIds, src => src.MapFrom(e => e.Employments.Select(e => e.UserId)))
                .ReverseMap();

            CreateMap<ProjectTask, TaskModel>()
                .ForMember(trg => trg.UpdatesIds, src => src.MapFrom(e => e.TaskUpdates.Select(e => e.Id)))
                .ForMember(trg => trg.UserIds, src => src.MapFrom(e => e.Assignments.Select(e => e.Employment.UserId)))
                .ReverseMap();

            CreateMap<TaskUpdate, UpdateModel>()
                .ForMember(trg => trg.UserId, src => src.MapFrom(e => e.Employment.UserId))
                .ReverseMap();

            CreateMap<UserProfile, ProfileModel>()
                .ForMember(trg => trg.Id, src => src.MapFrom(e => e.Id))
                .ForMember(trg => trg.Email, src => src.MapFrom(e => e.Email))
                .ForMember(trg => trg.UserName, src => src.MapFrom(e => e.UserName))
                .ForMember(trg => trg.UserId, src => src.MapFrom(e => e.UserId))
                .ForMember(trg => trg.CategoriesIds, src => src.MapFrom(e => e.Categories.Select(e => e.Id)))
                .ForMember(trg => trg.ProjectsIds, src => src.MapFrom(e => e.Employments.Select(e => e.ProjectId)))
                .ReverseMap();

            CreateMap<IdentityUser, UserModel>()
                .ForMember(trg => trg.Id, src => src.MapFrom(e => e.Id))
                .ForMember(trg => trg.Email, src => src.MapFrom(e => e.Email))
                .ForMember(trg => trg.UserName, src => src.MapFrom(e => e.UserName));

            CreateMap<UserProfile, UserModel>()
                .ForMember(trg => trg.Id, src => src.MapFrom(e => e.Id))
                .ForMember(trg => trg.Email, src => src.MapFrom(e => e.Email))
                .ForMember(trg => trg.UserName, src => src.MapFrom(e => e.UserName));
        }
    }
}
