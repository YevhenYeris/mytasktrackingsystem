import { HttpHeaders } from "@angular/common/http";

export const environment = {
  production: false,
  baseUrl: 'https://localhost:44369/api'
};

export const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true
};
