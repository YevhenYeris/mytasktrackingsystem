export enum ApiPaths {
  Categories = '/Categories',
  Projects = '/Projects',
  Roles = '/Roles',
  Users = '/Users',
  Statuses = '/Statuses',
  Tasks = '/Tasks'
}

export enum Claims {
  UserName = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
  Email = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
  UserId = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"
}