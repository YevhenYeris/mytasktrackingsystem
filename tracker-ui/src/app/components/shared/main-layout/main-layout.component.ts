import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = []

  constructor(public service: UserService,
              private router: Router) { }

  ngOnInit(): void {
  }

  logout() {
    this.subscriptions.push(this.service.logout().subscribe(res =>
      {
        this.router.navigateByUrl('/dashboard');
        location.reload();
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
