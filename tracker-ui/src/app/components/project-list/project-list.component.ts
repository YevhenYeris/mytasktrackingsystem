import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { forkJoin, Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { Project } from 'src/app/models/project';
import { ProjectFilter } from 'src/app/models/projectFilter';
import { ProjectService } from 'src/app/services/project/project.service';
import { UserService } from 'src/app/services/user/user.service';
import { Claims } from 'src/enums/api-paths';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  public projects: Project[] = []
  public indexes: Number[] = []
  private subscriptions: Subscription[] = []

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects(): void {
    let id = this.userService.getCurrentUser().id;
    let filter = new ProjectFilter();
    filter.userId = id;

    this.subscriptions.push(this.userService.getProjects(id, filter)
                                            .subscribe(res => this.projects = res));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
