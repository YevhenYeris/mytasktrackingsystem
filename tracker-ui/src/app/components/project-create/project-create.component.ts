import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Project } from 'src/app/models/project';
import { CategoryService } from 'src/app/services/category/category.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit, OnDestroy {

  categories: Category[] = []
  subscriptions: Subscription[] = [];

  constructor(public service: ProjectService,
              private router: Router,
              private userService: UserService,
              public categoryService: CategoryService) { }

  ngOnInit(): void {
    let userId = this.userService.getCurrentUser().id;
    this.getCategories(userId);
  }

  getCategories(id: Number) {
    this.subscriptions.push(this.userService.getCategories(id).subscribe((res) =>
    {
      this.categories = res;
    }));
  }

  onSubmit() {
    let project = new Project();
    project.projectName = this.service.createForm.value.Name;
    project.categoryId = this.service.createForm.value.CategoryId;
    project.color = this.service.createForm.value.Color;
    project.description = this.service.createForm.value.Description;
    project.dueTime = this.service.createForm.value.DueDate;
    this.subscriptions.push(this.service.postProject(project).subscribe());
    //this.service.createForm.reset();
    //this.router.navigateByUrl('/dashboard');
  }

  onCSubmit() {
    let id = this.userService.getCurrentUser().id;
    let category = new Category();
    category.categoryName = this.categoryService.createForm.value.Name;
    category.userId = id;
    this.subscriptions.push(this.categoryService.post(category).subscribe((res) =>
    {
      this.categories.push(res);
    }));
    this.categoryService.createForm.reset();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
