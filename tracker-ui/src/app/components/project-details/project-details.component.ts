import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Category } from 'src/app/models/category';
import { Project } from 'src/app/models/project';
import { ProjectTask } from 'src/app/models/projectTask';
import { User } from 'src/app/models/user';
import { UserRole } from 'src/app/models/userRole';
import { CategoryService } from 'src/app/services/category/category.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit, OnChanges {

  @Input()
  projectId: Number = 0

  @Output()
  loadingEmitter = new EventEmitter<boolean>();

  constructor(private service: ProjectService,
    private categoryService: CategoryService,
    private userService: UserService,) { }

  ngOnInit(): void {
    this.getProject();
  }

  public project: Project = new Project()
  public category: Category = new Category()
  public manager: User = new User()
  public tasks: ProjectTask[] = []
  public users: User[] = []
  public usersRoles: UserRole[] = []
  private subscriptions: Subscription[] = []

  public tasksIds: Map<number, ProjectTask> = new Map<number, ProjectTask>()
  public task: ProjectTask = new ProjectTask()

  getProject(): void {
    this.subscriptions.push(this.service.getSingle(this.projectId).pipe(first()).subscribe(project =>
    {
      this.project = project;
      this.getTasks();
      this.getCategory();
      this.loadingEmitter.emit(false);
    }));
  }

  getCategory(): void {
    if (this.project.categoryId != undefined)
    {
      this.subscriptions.push(this.categoryService.get(this.project.categoryId).pipe(first()).subscribe(res =>
        {
          this.category = res;
          this.getUser();
          this.getUserRoles();
        }));
    }
  }

  getTasks(): void {
    if (this.project.id != undefined)
    {
      this.subscriptions.push(this.service.getTasks(this.project.id).subscribe(res =>
        {
          this.tasks = res;
        }));
    }
  }

  getUsers(): void {
    if (this.project.id != undefined && this.project.id != 0)
    {
      this.subscriptions.push(this.service.getUsers(this.project.id).pipe(first()).subscribe(res => this.users = res));
    }
  }

  getUserRoles(): void {
    if (this.project.id != undefined && this.project.id != 0)
    {
      this.subscriptions.push(this.service.getUsersWithRoles(this.project.id).pipe(first()).subscribe(res => this.usersRoles = res));
    }
  }

  getUser(): void {
    if (this.category.userId != undefined)
    {
      this.subscriptions.push(this.userService.getUser(this.category.userId).pipe(first()).subscribe(res => this.manager = res));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ownsProject(): boolean {
    let id = this.userService.getCurrentUser()["id"];

    if (id && this.category)
      return id == this.category.userId;

    return false;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.projectId = changes['projectId'].currentValue;
    this.getProject();
  }

  loadTask(taskId: Number) {
    if (!this.tasksIds[taskId.valueOf()])
    this.subscriptions.push(this.service.getTask(this.projectId, taskId).pipe(first()).subscribe(res =>
      {
        this.tasksIds[taskId.valueOf()] = res;
      }));
    this.task = this.tasksIds[taskId.valueOf()];
  }

  getTask(taskId: Number): ProjectTask {
    return this.tasksIds[taskId.valueOf()];
  }
}
