import { Component, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Category } from 'src/app/models/category';
import { Project } from 'src/app/models/project';
import { ProjectTask } from 'src/app/models/projectTask';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';
import { CategoryService } from 'src/app/services/category/category.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { RoleService } from 'src/app/services/role/role.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css']
})
export class ProjectPageComponent implements OnInit, OnDestroy {

  public loading = false;

  public project: Project = new Project()
  public category: Category = new Category()
  public manager: User = new User()
  public tasks: ProjectTask[] = []
  public users: User[] = []
  private subscriptions: Subscription[] = []

  public projectId: Number = 0

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getProject();
  }

  getProject(): void {
    let id;
    this.subscriptions.push(this.route.params.subscribe(params =>
      {
        this.loading = true;
        id = Number.parseInt(params["projectId"]);
        if (Number.parseInt(id) != null)
          this.projectId = id;
      }))
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
