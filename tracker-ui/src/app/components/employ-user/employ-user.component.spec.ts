import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployUserComponent } from './employ-user.component';

describe('EmployUserComponent', () => {
  let component: EmployUserComponent;
  let fixture: ComponentFixture<EmployUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
