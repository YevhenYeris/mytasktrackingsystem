import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EmployUser } from 'src/app/models/employUser';
import { Role } from 'src/app/models/role';
import { ProjectService } from 'src/app/services/project/project.service';
import { RoleService } from 'src/app/services/role/role.service';

@Component({
  selector: 'app-employ-user',
  templateUrl: './employ-user.component.html',
  styleUrls: ['./employ-user.component.css']
})
export class EmployUserComponent implements OnInit, OnDestroy {

  public roles: Role[] = []

  @Input()
  projectId: Number = 0

  subscriptions: Subscription[] = []

  constructor(private roleService: RoleService,
              public service: ProjectService) { }

  ngOnInit(): void {
    this.getRoles();
  }

  getRoles(): void {
   this.subscriptions.push(this.roleService.getRoles().subscribe(res => this.roles = res));
  }

  onSubmit() {
    let employ = new EmployUser();
    employ.projectId = this.projectId;
    employ.roleId = this.service.employForm.value.Role;
    employ.userEmail = this.service.employForm.value.Email;
    employ.notify = this.service.employForm.value.Notify ? true : false;
    this.subscriptions.push(this.service.employUser(employ).subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
