import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProjectTask } from 'src/app/models/projectTask';
import { TaskStatus } from 'src/app/models/status';
import { ProjectService } from 'src/app/services/project/project.service';
import { TaskService } from 'src/app/services/task/task.service';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit, OnDestroy {

  @Input()
  projectId: Number
  private subscriptions: Subscription[] = []
  public statuses: TaskStatus[] = []

  constructor(public service: TaskService,
              private projectService: ProjectService,
              private router: Router) { }

  ngOnInit(): void {
    this.getStatuses();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

  getStatuses(): void {
    this.subscriptions.push(this.service.getStatuses().subscribe(res => this.statuses = res));
  }

  onSubmit(): void {
    let task: ProjectTask = new ProjectTask()

    task.dueTime = this.service.createForm.value.DueDate;
    task.taskName = this.service.createForm.value.Name;
    task.description = this.service.createForm.value.Description;
    task.taskStatusId = this.service.createForm.value.StatusId;
    this.subscriptions.push(this.projectService.createTask(this.projectId, task).subscribe());
    
    this.service.createForm.reset();
    this.router.navigateByUrl(`/dashboard/projects/${this.projectId}`);
  }
}
