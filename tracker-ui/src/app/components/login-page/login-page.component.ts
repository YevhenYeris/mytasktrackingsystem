import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  public register: boolean = false
  private subscriptіons: Subscription[] = []

  constructor(public service: UserService, private router: Router) { }

  onSubmit() {
    if (this.register)
    {
      this.subscriptіons.push(this.service.register().subscribe());
      this.service.registerForm.reset();
    }
    this.subscriptіons.push(this.service.login().subscribe((res) => {
      localStorage.setItem('token', res["access_token"]);
      localStorage.setItem('username', res["username"]);
    }));
    this.service.loginForm.reset();
    this.router.navigate(["/dashboard"]);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptіons.forEach(s => s.unsubscribe());
  }
}
