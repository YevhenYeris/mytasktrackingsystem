import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProjectTask } from 'src/app/models/projectTask';
import { TaskStatus } from 'src/app/models/status';
import { TaskUpdate } from 'src/app/models/taskUpdate';
import { TaskService } from 'src/app/services/task/task.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-task-page',
  templateUrl: './task-page.component.html',
  styleUrls: ['./task-page.component.css']
})
export class TaskPageComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = []

  @Input()
  task: ProjectTask = new ProjectTask()
  taskStatus: TaskStatus = new TaskStatus()
  updates: TaskUpdate[] = []

  constructor(public service: TaskService,
              private userService: UserService) { }

  ngOnInit(): void {
    if (this.task.id != undefined)
    {
      this.getStatus();
      this.getUpdates();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getStatus(): void {
    this.subscriptions.push(this.service.getStatus(this.task.taskStatusId).subscribe(res => this.taskStatus = res));
  }

  getUpdates(): void {
    this.subscriptions.push(this.service.getUpdates(this.task.id).subscribe(res => this.updates = res));
  }

  sendUpdate(): void {
    let update: TaskUpdate = new TaskUpdate();

    update.taskId = this.task.id;
    update.userId = this.userService.getCurrentUser().id;
    update.description = this.service.updateForm.value.Description;

    this.service.updateForm.reset();
    this.subscriptions.push(this.service.sendUpdate(this.task.id, update).subscribe(res =>
    {
        this.updates.push(res);
    }));
  }
}
