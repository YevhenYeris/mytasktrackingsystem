import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AdminModule } from './admin/admin.module';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { ProjectPageComponent } from './components/project-page/project-page.component';
import { MainLayoutComponent } from './components/shared/main-layout/main-layout.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

const routes: Routes = [
  {
    path: '', component: MainLayoutComponent, children: [
      {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
      {path: 'dashboard', component: HomePageComponent, children: [
        {path: 'projects/:projectId', component: ProjectPageComponent},
        {path: 'createProject', component: ProjectCreateComponent}
      ]},
      {path: 'login', component: LoginPageComponent},
      {path: 'myProfile', component: UserProfileComponent}
    ]
  },
  {
    path: 'admin', loadChildren: () => AdminModule
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
