import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { EmployUser } from 'src/app/models/employUser';
import { Project } from 'src/app/models/project';
import { ProjectFilter } from 'src/app/models/projectFilter';
import { ProjectTask } from 'src/app/models/projectTask';
import { User } from 'src/app/models/user';
import { UserRole } from 'src/app/models/userRole';
import { ApiPaths } from 'src/enums/api-paths';
import { environment, httpOptions } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private baseUrl: string = `${environment.baseUrl}${ApiPaths.Projects}`

  constructor(private fb: FormBuilder, private httpClient: HttpClient, private cookieService: CookieService) { }

  createForm = this.fb.group({
    Name :['', Validators.required],
    Color :['', Validators.required],
    Description :['', Validators.required],
    DueDate :['', Validators.required],
    CategoryId :['', Validators.required]
  })

  employForm = this.fb.group({
    Email: ['', [Validators.required, Validators.email]],
    Role: ['', Validators.required],
    Notify: ['', Validators.required]
  })

  getAll(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(this.baseUrl, httpOptions);
  }

  postProject(project: Project): Observable<Project> {
    return this.httpClient.post<Project>(`${this.baseUrl}`, project, httpOptions);
  }

  getSingle(id: Number): Observable<Project> {
    let url = `${this.baseUrl}/${id}`;
    return this.httpClient.get<Project>(url, httpOptions);
  }

  getByFilter(filter: ProjectFilter): Observable<Project[]> {
    for (let item in filter)
    {
      filter[item] = filter[item] != undefined ? filter[item] : "0";
    }
    return this.httpClient.get<Project[]>(`${this.baseUrl}/?UserId=${filter.userId}&CategoryId=${filter.categoryId}&ManagerId=${filter.managerId}`, httpOptions);
  }

  getTasks(projectId: Number): Observable<ProjectTask[]> {
    return this.httpClient.get<ProjectTask[]>(`${this.baseUrl}/${projectId}/tasks`, httpOptions);
  }

  getUsers(projectId: Number): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.baseUrl}/${projectId}/users`, httpOptions);
  }

  employUser(employ: EmployUser) {
    return this.httpClient.post(`${this.baseUrl}/${employ.projectId}/users`, employ, httpOptions);
  }

  getTask(projectId: Number, taskId: Number): Observable<ProjectTask> {
    return this.httpClient.get<ProjectTask>(`${this.baseUrl}/${projectId}/tasks/${taskId}`, httpOptions);
  }

  createTask(projectId: Number, task: ProjectTask): Observable<ProjectTask> {
    task.projectId = projectId;
    return this.httpClient.post<ProjectTask>(`${this.baseUrl}/${projectId}/tasks`, task, httpOptions);
  }

  getUsersWithRoles(projectId: Number): Observable<UserRole[]> {
    return this.httpClient.get<UserRole[]>(`${this.baseUrl}/${projectId}/usersRoles`, httpOptions);
  }
}
