import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { TaskStatus } from 'src/app/models/status';
import { TaskUpdate } from 'src/app/models/taskUpdate';
import { ApiPaths } from 'src/enums/api-paths';
import { environment, httpOptions } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private statusesBaseUrl = `${environment.baseUrl}${ApiPaths.Statuses}`
  private baseUrl = `${environment.baseUrl}${ApiPaths.Tasks}`;

  constructor(private httpClient: HttpClient, private fb: FormBuilder) { }

  createForm = this.fb.group({
    Name :['', Validators.required],
    Description :['', Validators.required],
    DueDate :['', Validators.required],
    StatusId :['', Validators.required]
  })

  updateForm = this.fb.group({
    Description :['', [Validators.required, Validators.minLength(15)]]
  })

  getStatus(statusId: Number): Observable<TaskStatus> {
    return this.httpClient.get<TaskStatus>(`${this.statusesBaseUrl}/${statusId}`, httpOptions);
  }

  getStatuses(): Observable<TaskStatus[]> {
    return this.httpClient.get<TaskStatus[]>(`${this.statusesBaseUrl}`, httpOptions);
  }

  getUpdates(taskId: Number): Observable<TaskUpdate[]> {
    return this.httpClient.get<TaskUpdate[]>(`${this.baseUrl}/${taskId}/updates`, httpOptions);
  }

  sendUpdate(taskId: Number, update: TaskUpdate): Observable<TaskUpdate> {
    update.taskId = taskId;
    return this.httpClient.post<TaskUpdate>(`${this.baseUrl}/${taskId}/updates`, update, httpOptions);
  }

}
