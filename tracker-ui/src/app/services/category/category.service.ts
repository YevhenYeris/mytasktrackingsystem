import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Category } from 'src/app/models/category';
import { ApiPaths } from 'src/enums/api-paths';
import { environment, httpOptions } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private baseUrl: string = `${environment.baseUrl}${ApiPaths.Categories}`

  createForm = this.fb.group({
    Name :['', [Validators.required, Validators.minLength(5)]]
  })

  constructor(private fb: FormBuilder, private httpClient: HttpClient) { }

  post(category: Category): Observable<Category> {
    return this.httpClient.post<Category>(this.baseUrl, category, httpOptions);
  }

  get(categoryId: Number): Observable<Category> {
    return this.httpClient.get<Category>(`${this.baseUrl}/${categoryId}`, httpOptions);
  }
}
