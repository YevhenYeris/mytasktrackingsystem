import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import jwtDecode from "jwt-decode";
import { Observable } from "rxjs";
import { Category } from "src/app/models/category";
import { Project } from "src/app/models/project";
import { ProjectFilter } from "src/app/models/projectFilter";
import { User } from "src/app/models/user";
import { ApiPaths, Claims } from "src/enums/api-paths";
import { environment, httpOptions } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = `${environment.baseUrl}${ApiPaths.Users}`

  constructor(private fb:FormBuilder,
              private http: HttpClient,
              private router: Router) { }

  registerForm = this.fb.group({
     UserName :['', Validators.required],
      Email :['', [Validators.required, Validators.email]],
      Passwords : this.fb.group({
      Password :['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword :['', [Validators.required]]
    },
    {validator: this.comparePasswords})
  })

  loginForm = this.fb.group({
    Name :['', Validators.required],
    Password :['', [Validators.required, Validators.minLength(6)]]
  })

  comparePasswords(fg: FormGroup) {
    let cPassword = fg.get('ConfirmPassword');

    if (cPassword.errors == null || 'passwordMismatch' in cPassword.errors) {
      if (fg.get('Password').value != cPassword.value) {
        cPassword.setErrors({passwordMismatch: true});
      }
      else {
        cPassword.setErrors(null);
      }
    }
  }

  register() {
    var body = {
      Email : this.registerForm.value.Email,
      UserName : this.registerForm.value.UserName,
      Password : this.registerForm.value.Passwords.Password
    }
    return this.http.post(`${this.baseUrl}/register`, body);
  }

  login(): Observable<any> {
    var body = {
      UserName: this.loginForm.value.Name,
      Password: this.loginForm.value.Password
    }
    this.router.navigateByUrl('/dashboard');
    location.reload();
    return this.http.post(`${this.baseUrl}/login`, body, httpOptions);
  }

  logout(): Observable<any> {
    localStorage.removeItem("token");
    return this.http.delete(this.baseUrl, httpOptions);
  }

  isUserSigned(): boolean {
    return localStorage.getItem("token") != null;
  }

  getCategories(userId: Number): Observable<Category[]> {
    return this.http.get<Category[]>(`${this.baseUrl}/${userId}/categories`);
  }

  getUser(userId: Number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${userId}`);
  }

  getProjects(userId: Number, filter: ProjectFilter): Observable<Project[]> {
    for (let item in filter)
    {
      filter[item] = filter[item] != undefined ? filter[item] : "0";
    }
    return this.http.get<Project[]>(`${this.baseUrl}/${userId}/projects?UserId=${filter.userId}&CategoryId=${filter.categoryId}&ManagerId=${filter.managerId}&WithCompletion=true`, httpOptions);
  }

  getProject(userId: Number, projectId: Number): Observable<Project> {
    return this.http.get<Project>(`${this.baseUrl}/${userId}/projects/${projectId}`, httpOptions);
  }

  getTasks(userId: Number, projectId: Number): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseUrl}/projects/${projectId}/tasks`, httpOptions);
  }

  getCurrentUser(): User {
    let token = localStorage["token"];
    let user = new User();

    if (token)
    {
    user.userName = jwtDecode(token)[Claims.UserName][0];
    user.email = jwtDecode(token)[Claims.Email][1];
    user.id = jwtDecode(token)[Claims.UserId][2];
    }

    return user;
  }
}