import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Role } from 'src/app/models/role';
import { ApiPaths } from 'src/enums/api-paths';
import { environment, httpOptions } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private baseUrl: string = `${environment.baseUrl}${ApiPaths.Roles}`

  constructor(private httpClient: HttpClient) { }

  getRoles(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(`${this.baseUrl}`, httpOptions);
  }
}
