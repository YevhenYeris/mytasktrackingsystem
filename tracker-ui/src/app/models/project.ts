export class Project {
  id: Number;
  projectName: string;
  color: string;
  description: string;
  startTime: Date;
  dueTime: Date;
  categoryId: Number; 
  completion: Number;
}
