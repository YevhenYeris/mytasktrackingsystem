export class TaskUpdate {
  id: Number;
  userId: Number;
  taskId: Number;
  description: string = "";
  updateTime: Date = new Date();
}