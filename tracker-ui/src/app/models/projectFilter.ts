export class ProjectFilter {
  userId: Number = 0;
  managerId: Number = 0;
  categoryId: Number = 0;
  projectName: string = "";
  withCompletion: boolean = false;
}