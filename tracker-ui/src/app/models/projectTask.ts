export class ProjectTask {
  id: Number;
  taskName: string;
  description: string;
  projectId: Number;
  taskStatusId: Number;
  isFinished: boolean;
  startTime: Date;
  finishTime: Date;
  dueTime: Date;
}