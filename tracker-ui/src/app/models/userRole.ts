export class UserRole {
  userId: Number;
  roleId: Number;
  userName: string;
  roleName: string;
  roleColor: string;
}