export class EmployUser {
  userEmail: string;
  projectId: Number;
  roleId: Number;
  notify: boolean;
}