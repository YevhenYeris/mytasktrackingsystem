import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './components/shared/main-layout/main-layout.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { ProjectPageComponent } from './components/project-page/project-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { UserService } from './services/user/user.service';
import { ProjectService } from './services/project/project.service';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { EmployUserComponent } from './components/employ-user/employ-user.component';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { TaskPageComponent } from './components/task-page/task-page.component';
import { TaskCreateComponent } from './components/task-create/task-create.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    HomePageComponent,
    ProjectPageComponent,
    LoginPageComponent,
    ProjectListComponent,
    UserProfileComponent,
    ProjectCreateComponent,
    EmployUserComponent,
    TaskPageComponent,
    TaskCreateComponent,
    ProjectDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([{
      path: 'createProject',
      component: ProjectCreateComponent,
      canActivate: [AuthGuard]
    }])
  ],
  providers: [
    UserService,
    ProjectService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
